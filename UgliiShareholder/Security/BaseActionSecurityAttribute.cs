﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using log4net;

namespace UgliiShareholder.Security
{
    /// <summary>
    /// Base class for action security attributes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public abstract class BaseActionSecurityAttribute : ActionFilterAttribute, IAuthorizationFilter
    {
        public static ILog LogManager = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //private const string accessDeniedPath = "~/Error/AccessDenied.aspx";

        /// <summary>
        /// The authorization result values
        /// </summary>
        protected enum AuthResult
        { 
            /// <summary>
            /// Indicates that the user has access to the requested resource
            /// </summary>
            OK,
            /// <summary>
            /// Indicates that the user is not logged in and does not have access to the requested resource
            /// </summary>
            NotLoggedIn,
            /// <summary>
            /// Indicates that the user does not have access to the requested resource
            /// </summary>
            NotAuthorized
        }
      
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            
            AuthResult authResult = Authorize(filterContext);
						string redirectOnSuccess;
						string redirectUrl;
            switch(authResult)
            {
                case AuthResult.OK:
                    break;
                case AuthResult.NotLoggedIn:
                    redirectOnSuccess = filterContext.HttpContext.Request.Url.PathAndQuery;//filterContext.HttpContext.Request.Url.AbsolutePath;
                    redirectUrl = string.Format("?ReturnUrl={0}",redirectOnSuccess);
                    string loginUrl = FormsAuthentication.LoginUrl + redirectUrl;
                    LogManager.Info("User not logged in, redirecting to login page");
                    filterContext.Result = new RedirectResult(loginUrl);
					break;
                case AuthResult.NotAuthorized:
				    redirectOnSuccess = filterContext.HttpContext.Request.Url.PathAndQuery;//filterContext.HttpContext.Request.Url.AbsolutePath;
				    redirectUrl = string.Format("?ReturnUrl={0}", redirectOnSuccess);
                    LogManager.Info("User not Authorized, redirecting to Unauthorised page");
                    filterContext.Result = new RedirectResult(HttpContext.Current.Request.ApplicationPath + "Account/UnauthorisedAccess" + redirectUrl + "&so=" + filterContext.ActionDescriptor.ControllerDescriptor.ControllerName + "/" + filterContext.ActionDescriptor.ActionName);
				    break;
                default: 
                    throw new HttpException(403, "Access denied: " + filterContext.HttpContext.Request.RawUrl);
            }

        }

        protected abstract AuthResult Authorize(AuthorizationContext filterContext);
    }
}