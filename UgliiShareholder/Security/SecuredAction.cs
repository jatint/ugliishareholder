﻿using System;
using System.Web.Mvc;
using System.Web;
using UgliiShareholder.Controllers;
using System.Web.Routing;
using UgliiShareholder.Core;

namespace UgliiShareholder.Security
{
    /// <summary>
    /// Tags an action as a standard SISS secured operation.
    /// </summary>
    public class SecuredAction : BaseActionSecurityAttribute
    {
        protected override AuthResult Authorize(AuthorizationContext filterContext)
        {
           //For CORS calls from other domains, there is no auth cookies in the request so we don't know who the user is yet,
           //so allow the OPTIONS head to at least pass.
           if(filterContext.HttpContext.Request.HttpMethod == "OPTIONS"){
              return AuthResult.OK; 
           }
            LogManager.Info("Authorize Attribute");
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated )
            {
                LogManager.Info( "User Not Logged In");
                return AuthResult.NotLoggedIn;
            }

            // temp
            //return AuthResult.OK;
            // end

            //Type controllerType = filterContext.Controller.ControllerContext.Controller.GetType();
            LogManager.Info("User Logged In");
            string operationKey = GetOperationKey(filterContext.RouteData.Values);

            if (!(filterContext.Controller as BaseController).HasAccessToOperation(operationKey))
            {
                LogManager.Info( "User Not Authorized:"+operationKey);
                return AuthResult.NotAuthorized;
            }
            LogManager.Info( "User Authorized:" + operationKey);
            return AuthResult.OK;
        }

        private string GetOperationKey(RouteValueDictionary routeValues)
        {
            //RequireValidRoute(routeValues);
            string controller = routeValues["controller"] as string; // GetControllerName(routeValues);
            string action = routeValues["action"] as string; //GetActionName(routeValues);

            if (!(!string.IsNullOrEmpty(controller) && !string.IsNullOrEmpty(action)))
            {
                throw new Exception("HACK:Route is not valid");
            }

            string operationKey = string.Format("/{0}/{1}", controller, action);
            return operationKey;
        }


    }
}