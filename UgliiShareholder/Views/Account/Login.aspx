﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Unauthenticated.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="form">
        <form method="post">
            
            <% Response.Write(Html.ValidationSummary("", new { @class = "message error" })); %>

            <fieldset class="required">
                <label>Username</label>
                <input tabindex="0" type="text" name="username" id="username" value="<%: ViewData["username"] as string %>" />
                <br />
                <label>Password</label>
                <input type="password" tabindex="0" name="password" id="password" />
                <br />
                <%
                    Response.Write(Html.CheckBox("rememberMe"));
                %>
                <label>Remember Me</label>

                <br />
                <br />
                <div class="FormButton">
                    <button type="submit" class="float-left" title="Login">Login</button>
                </div>
                <br />
                <div>
                    <a href="<%=Request.ApplicationPath%>LostPassword/ResetLostPassword">Lost Password ?</a> | <a href="<%=Request.ApplicationPath%>LostPassword/ResetLostPassword">Register</a>
                </div>
            </fieldset>
        </form>
    </div>
    <script>
        $(document).ready(function () {
            $("#username").select();
        });
    </script>
</asp:Content>
