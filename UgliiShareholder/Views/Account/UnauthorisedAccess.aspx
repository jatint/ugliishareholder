﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Unauthenticated.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

     <%Response.Write("You have attempted to access a page that you are not authorized to." + ViewData["AttemptedPage"] );%>

    <br />
    <br />

<div class="formbutton"><a class="button" href="/Account/LogOff">{SID:Account_LogOut}</a></div>

</asp:Content>
