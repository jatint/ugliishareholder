﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/StandardListUgliiGrid.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phHead" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="phTitle" runat="server">
    Secured operation Management
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainPage" runat="server">

    <style type="text/css">
        ul.controls-buttons li a.help {
            margin-left: 8px;
        }

            ul.controls-buttons li a.help img {
                vertical-align: middle;
                margin-top: -6px;
            }

        #grid {
            overflow: auto;
        }

        span.mandatory {
            float: left;
            margin-top: 10px;
            font-size: 18px;
            margin-left: 2px;
        }
    </style>

    <h1>Manage Shareholder</h1>

    <div class="task">
        <div class="block-controls">
            <div style="display: none;" id="GridMessage" class="message success">Data was Succesfully Saved</div>
            <% Html.RenderPartial("UgliiGridPaging", "ddPage"); %>
            <ul class="controls-buttons">
                <li><a href="#" id="addMarketingLink" class="add small" onclick="javascript: new SecuredOperations(null)._ShowInsertEdit(null)">Insert</a></li>
            </ul>
        </div>
        <div id="grid" style="display: none;">
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th class="sorting filtering"></th>

                        <th class="sorting filtering">{column:OperationKey}<span class="UGColName">Operation Key</span></th>

                        <th class="sorting filtering">{column:Description}<span class="UGColName">Description</span></th>

                        <th class="sorting filtering">{column:IsAdmin}<span class="UGColName">Is Admin</span></th>
                        
                    </tr>
                </thead>
                <tbody id="hasLinks">
                    <tr>
                        <td>
                            <div class="button menu-opener medium-margin">
                                Action
                                <div class="menu-arrow">
                                    <img height="16" width="16" src="{CONFIG:CONSTELLATIONPATH}/images/menu-open-arrow.png" />
                                </div>
                                <div class="menu">
                                    <ul style="opacity: 1;">
                                        {ufield:ActionRequest}
                                    </ul>
                                </div>
                            </div>
                        </td>

                        <td style="word-break: break-all!important;">{field:operationkey}
                        </td>

                        <td style="word-break: break-all!important;">{field:description}
                        </td>

                        <td style="word-break: break-all!important;">{field:IsAdminFunction}
                        </td>
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>
    </div>

    <script type="text/javascript" src="../Scripts/ControllersJS/SecuredOperationsMgmt.js"></script>
</asp:Content>

