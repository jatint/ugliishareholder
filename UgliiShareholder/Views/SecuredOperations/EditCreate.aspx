﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <ul class="tabs">
                <li data-bind="css: { current: _CurrentTab() == 'details' }"><a href="#details" data-bind="    click: function () { _SetTab('details') }">Details</a></li>
            </ul>

            <div class="tabs-content">
                <div id="details" data-bind="visible: _CurrentTab() == 'details'">
                    <div class="oneColumn">
                        <label class="required">Operation Key</label>
                        <input type="text" data-bind="value: OperationKey" id="OperationKey" maxlength="50" />
                    </div>

                    <div class="oneColumn">
                        <label class="required">Description</label>
                        <input type="text" data-bind="value: Description" maxlength="150" id="Description" />
                    </div>
                    <div class="oneColumn">
                        <label>
                            Admin</label>
                        <input type="checkbox" data-bind="checked: IsAdmin" />
                    </div>

                </div>
            </div>

        </div>
    </fieldset>

</asp:Content>
