﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<dynamic>" %>

  <%-- begin constellation scripts --%>
  <script type="text/javascript" src="../Scripts/js/html5.js"></script> 
  <script type="text/javascript" src="../Scripts/js/jquery-1.8.9.min.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery-1.8.0.min.js"></script>

  <script type="text/javascript" src="../Scripts/js/jquery.accessibleList.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery.blockUI.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery.togglebutton.js"></script>
  
  <script type="text/javascript" src="../Scripts/js/jquery.easyAccordion.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery.tip.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery.hashchange.js"></script>
  
  <script type="text/javascript" src="../Scripts/js/jquery.contextMenu.js"></script>
  <script type="text/javascript" src="../Scripts/js/jquery.modal.js"></script>

  <script type="text/javascript" src="../Scripts/js/knockout-3.0.0.js"></script>
  <script type="text/javascript" src="../Scripts/js/common.js"></script>
  <script type="text/javascript" src="../Scripts/js/standard.js"></script>
  

  <%-- end constellation scripts --%>