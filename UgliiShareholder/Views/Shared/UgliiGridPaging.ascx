﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<string>" %>
<div class="controls-buttons ">
    
    <div class="sub-hover">
        <div class="spTotalRecords" >&nbsp;</div>
        <a href="#" class="control-first pLL with-tip" title="First Page">&lt;&lt;</a> <a href="#"
            class="control-prev pL with-tip" title="Prev Page">&lt;</a> <span>Page 
                <select class="small " id="<%=Model %>">
                    <option value="0" >1</option>
                </select>
                <label class="displayPages">
                </label>
            </span><a href="#" class="control-next pR with-tip" title="Next Page">&gt;</a>
        <a href="#" class="control-last pRR with-tip" title="Last Page">&gt;&gt;</a>
    </div>
</div>
