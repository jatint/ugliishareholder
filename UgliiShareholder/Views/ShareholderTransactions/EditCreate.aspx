﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <ul class="tabs">
                <li data-bind="css: { current: _CurrentTab() == 'details' }"><a href="#details" data-bind="    click: function () { _SetTab('details') }">Details</a></li>
                <li data-bind="css: { current: _CurrentTab() == 'LoginDetails' }"><a href="#LoginDetails" data-bind="    click: function () { _SetTab('LoginDetails') }">Login Details</a></li>
            </ul>

            <div class="tabs-content">
                <div id="details" data-bind="visible: _CurrentTab() == 'details'">
                    <div class="twoColumns">
                        <label class="required">Shareholder Name</label>
                        <input type="text" data-bind="value: ShareholderName, attr: { disabled: _Mode() == 2 ? 'disabled' : null}" maxlength="150" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">Beneficiary</label>
                        <input type="text" data-bind="value: Beneficiary, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="150" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Mobile Phone</label>
                        <input type="text" data-bind="value: MobilePhone, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="20" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Fixed Line Phone</label>
                        <input type="text" data-bind="value: FixedLinePhone, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="20" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Alternate Phone</label>
                        <input type="text" data-bind="value: AlternatePhone, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="20" />
                    </div>
                    <div class="twoColumns">
                        <label>
                            Fax No</label>
                        <input type="text" data-bind="value: FaxNo, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="20" />
                    </div>
                    <div class="twoColumns">
                        <label>C/o</label>
                        <input type="text" data-bind="value: CO, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="50" />
                    </div>
                    <div class="twoColumns">
                        <label>Street Address</label>
                        <input type="text" data-bind="value: StreetAddress, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="250" />
                    </div>
                    <div class="twoColumns">
                        <label>Town</label>
                        <input type="text" data-bind="value: Town, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="50" />
                    </div>
                    <div class="twoColumns">
                        <label>State</label>
                        <input type="text" data-bind="value: CO, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="30" />
                    </div>
                    <div class="twoColumns">
                        <label>Postcode</label>
                        <input type="text" data-bind="value: Postcode, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="10" />
                    </div>
                    <div class="twoColumns">
                        <label>Country</label>
                        <input type="text" data-bind="value: Country, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="150" />
                    </div>
                    <div class="twoColumns">
                        <label>Postal Address</label>
                        <input type="text" data-bind="value: PostalAddress, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="250" />
                    </div>
                </div>
                <div id="LoginDetails" data-bind="visible: _CurrentTab() == 'LoginDetails'">
                    <div data-bind="with: ShareholderLogin" style="width:100%;">
                        <div class="block-controls" data-bind="attr: { style: _Mode() == 2 ? 'display:none;' : null }">
                            <ul class="controls-buttons">
                                <li><a href="#" id="" class="add small" data-bind="click: function (data, event) { _ShowInsertEdit(null, 0, $parents[0]) }">Register New Login</a></li>
                                <li><a href="#" id="" class="add small" data-bind="click: function (data, event) { _AddExistingUser($parents[0]) }">Add Existing Login</a></li>
                            </ul>
                        </div>
                        <table style="width:100%">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Email Address</th>
                                    <th>Is Admin</th>
                                    <th>Receive Newsletter</th>
                                </tr>
                            </thead>
                            <tbody data-bind="attr: { style: ID == '00000000-0000-0000-0000-000000000000' ? 'display:none':null }">
                                <tr>
                                    <td data-bind="text: UserName"></td>
                                    <td data-bind="text: EmailAddress"></td>
                                    <td data-bind="text: IsAdmin"></td>
                                    <td data-bind="text: ReceiveNewsletter"></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="attr: { style: ID != '00000000-0000-0000-0000-000000000000' ? 'display:none' : null }">
                                <tr>
                                    <td colspan="4" >No User Info</td>
                                </tr>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

</asp:Content>
