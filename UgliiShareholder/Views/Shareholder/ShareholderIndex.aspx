﻿<%@ Page Title="" Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<div>
    <style type="text/css">
        ul.controls-buttons li a.help {
            margin-left: 8px;
        }

            ul.controls-buttons li a.help img {
                vertical-align: middle;
                margin-top: -6px;
            }

        #grid {
            overflow: auto;
        }

        span.mandatory {
            float: left;
            margin-top: 10px;
            font-size: 18px;
            margin-left: 2px;
        }
    </style>

    <h1>Manage Shareholder</h1>

    <div class="task">
        <div class="block-controls">
            <div style="display: none;" id="GridMessage" class="message success">Data was Succesfully Saved</div>
            <% Html.RenderPartial("UgliiGridPaging", "ddPage2"); %>
            <ul class="controls-buttons">
                <li><a href="#" id="addMarketingLink" class="add small" onclick="javascript: new ShareholderDetails(null)._ShowInsertEdit(null, 0)">Register New Shareholder</a></li>
            </ul>
        </div>
        <div id="grid" style="display: none;">
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th class="sorting filtering"></th>

                        <th class="sorting filtering">{column:ShareholderName}<span class="UGColName">Shareholder Name</span></th>

                        <th class="sorting filtering">{column:MobilePhone}<span class="UGColName">MobilePhone</span></th>

                        <th class="sorting filtering">{column:Shareholder_login.EmailAddress}<span class="UGColName">Email</span></th>

                        <th class="sorting filtering">{column:State}<span class="UGColName">State</span></th>

                        <th class="sorting filtering">{column:Country}<span class="UGColName">Country</span></th>

                        <th class="sorting filtering"><span class="UGColName">Postal Address</span></th>
                    </tr>
                </thead>
                <tbody id="hasLinks">
                    <tr>
                        <td>
                            <div class="button menu-opener medium-margin">
                                Action
                                <div class="menu-arrow">
                                    <img height="16" width="16" src="{CONFIG:CONSTELLATIONPATH}/images/menu-open-arrow.png" />
                                </div>
                                <div class="menu">
                                    <ul style="opacity: 1;">
                                        <li class="icon_edit">
                                            <a href="#" onclick="new ShareholderDetails(null)._ShowInsertEdit('{field:Id}', 1)" title="Edit" class="with-tip">Edit</a>
                                        </li>
                                        <li class="icon_search">
                                            <a href="#" onclick="new ShareholderDetails(null)._ShowInsertEdit('{field:Id}', 2)" title="Edit" class="with-tip">View</a>
                                        </li>
                                        <li class="icon_delete">
                                            <a href="#" onclick="new ShareholderDetails(null)._DeleteData('{field:Id}','{field:ShareholderName}')" title="Delete" class="with-tip">Delete</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>

                        <td style="word-break: break-all!important;">{field:ShareholderName}
                        </td>

                        <td style="word-break: break-all!important;">{field:MobilePhone}
                        </td>

                        <td style="word-break: break-all!important;">{field:EmailAddress}
                        </td>
                        <td style="word-break: break-all!important;">{field:State}
                        </td>
                        <td>{field:Country}
                        </td>
                        <td style="word-break: break-all!important;">{field:PostalAddress}
                        </td>
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>
    </div>
</div>



