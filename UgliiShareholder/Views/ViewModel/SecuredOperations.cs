﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UgliiShareholder.Core;
using UgliiShareholder.DataModel;

namespace UgliiShareholder.ViewModel
{
    public class SecuredOperationsModel : ModelWithValidation
    {
          /// <summary>
        /// Empty constructor 
        /// </summary>
        public SecuredOperationsModel() { }

        /// <summary>
        /// Loads Secured Operation to model
        /// </summary>
        /// <param name="DB">Data Context</param>
        /// <param name="Id">Secured Operation id</param>
        public SecuredOperationsModel(ShareHolderDataContext DB, Guid Id)
        {
            if (DB != null && Id != Guid.Empty)
            {
                SecuredOperation objSecuredOp= DB.SecuredOperations.FirstOrDefault(sh => sh.ID == Id);

                if (objSecuredOp != null)
                {
                    this.Id = objSecuredOp.ID;
                    this.Deleted = objSecuredOp.Deleted;
                    this.Description = objSecuredOp.description;
                    this.IsAdmin = objSecuredOp.IsAdminFunction;
                }
            }
        }

        /// <summary>
        /// Returns Database Model 
        /// </summary>
        /// <returns></returns>
        public SecuredOperation GetDatabaseModel(SecuredOperation objSecuredOp = null)
        {
            if (objSecuredOp == null)
                objSecuredOp = new SecuredOperation();

            objSecuredOp.ID = this.Id;
            objSecuredOp.operationkey = this.OperationKey;
            objSecuredOp.description = this.Description;
            objSecuredOp.Deleted = this.Deleted;
            objSecuredOp.IsAdminFunction = this.IsAdmin;

            return objSecuredOp;
        }

        /// <summary>
        /// Get/Set Shareholder Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Get/Set Opeation Key 
        /// </summary>
        [StringLength(50, ErrorMessage = "Operation Key cannot be more than 50 characters!")]
        [Required(ErrorMessage = "Operation Key cannot be empty!")]
        public string OperationKey { get; set; }

        /// <summary>
        /// Get/Set Description
        /// </summary>
        [StringLength(150, ErrorMessage = "Description cannot be more than 150 characters!")]
        [Required(ErrorMessage = "Description cannot be empty!")]
        public string Description { get; set; }

        /// <summary>
        /// Get/Set Is Admin
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Get/set Deleted
        /// </summary>
        public bool Deleted { get; set; }

    }
}