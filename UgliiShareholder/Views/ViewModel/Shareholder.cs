﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using UgliiShareholder.Core;
using UgliiShareholder.Core.Attributes;
using UgliiShareholder.DataModel;

namespace UgliiShareholder.ViewModel
{
    /// <summary>
    /// Shareholder details Model
    /// </summary>
    public class ShareHolderDetailModel : ModelWithValidation
    {
        /// <summary>
        /// Empty constructor 
        /// </summary>
        public ShareHolderDetailModel() { }

        /// <summary>
        /// Loads Shareholder information to model
        /// </summary>
        /// <param name="DB">Data Context</param>
        /// <param name="Id">Share holder details id</param>
        public ShareHolderDetailModel(ShareHolderDataContext DB, Guid Id)
        {
            if (DB != null && Id != Guid.Empty)
            {
                Shareholder_Detail objShareHolder = DB.Shareholder_Details.FirstOrDefault(sh => sh.ID == Id);

                if (objShareHolder != null)
                {
                    this.Id = objShareHolder.ID;
                    this.AlternatePhone = objShareHolder.AlternatePhone;
                    this.Beneficiary = objShareHolder.Beneficiary;
                    this.CO = objShareHolder.CO;
                    this.Country = objShareHolder.Country;
                    this.FaxNo = objShareHolder.FaxNo;
                    this.FixedLinePhone = objShareHolder.FixedLinePhone;
                    this.MobilePhone = objShareHolder.MobilePhone;
                    this.PostalAddress = objShareHolder.PostalAddress;
                    this.Postcode = objShareHolder.Postcode;
                    this.ShareholderName = objShareHolder.ShareholderName;
                    this.State = objShareHolder.State;
                    this.StreetAddress = objShareHolder.StreetAddress;
                    this.Town = objShareHolder.Town;
                    this.ShareholderLogin = new ShareholderLoginModel(objShareHolder.Shareholder_Login);
                }
            }
        }

        /// <summary>
        /// Returns Database Model 
        /// </summary>
        /// <returns></returns>
        public Shareholder_Detail GetDatabaseModel(Shareholder_Detail objShareDetails = null)
        {
            if (objShareDetails == null)
                objShareDetails = new Shareholder_Detail();

            objShareDetails.ID = this.Id;
            objShareDetails.AlternatePhone = this.AlternatePhone;
            objShareDetails.Beneficiary = this.Beneficiary;
            objShareDetails.CO = this.CO;
            objShareDetails.Country = this.Country;
            objShareDetails.FaxNo = this.FaxNo;
            objShareDetails.FixedLinePhone = this.FixedLinePhone;
            objShareDetails.MobilePhone = this.MobilePhone;
            objShareDetails.PostalAddress = this.PostalAddress;
            objShareDetails.Postcode = this.Postcode;
            objShareDetails.ShareholderName = this.ShareholderName;
            objShareDetails.State = this.State;
            objShareDetails.StreetAddress = this.StreetAddress;
            objShareDetails.Town = this.Town;
            
            return objShareDetails;
        }

        /// <summary>
        /// Get/Set Shareholder Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Get/Set Shareholder name  (maximum character  = 150)
        /// </summary>
        [StringLength(150, ErrorMessage = "Shareholder name cannot be more than 150 characters!")]
        [Required(ErrorMessage = "Shareholder name cannot be empty!")]
        public string ShareholderName { get; set; }

        /// <summary>
        /// Get/Set Beneficiary
        /// </summary>
        [StringLength(150, ErrorMessage = "Beneficiary cannot be more than 150 characters!")]
        public string Beneficiary { get; set; }

        /// <summary>
        /// Get/set Mobile Phone
        /// </summary>
        [StringLength(20, ErrorMessage = "Mobile Phone no cannot be more than 20 characters!")]
        public string MobilePhone { get; set; }

        /// <summary>
        /// Get/set Fixed Line Phone
        /// </summary>
        [StringLength(20, ErrorMessage = "Fixed line Phone no cannot be more than 20 characters!")]
        public string FixedLinePhone { get; set; }

        /// <summary>
        /// Get/set Alternate Phone
        /// </summary>
        [StringLength(20, ErrorMessage = "Alternate Phone no cannot be more than 20 characters!")]
        public string AlternatePhone { get; set; }


        /// <summary>
        /// Get/set Fax No
        /// </summary>
        [StringLength(20, ErrorMessage = "Fax no cannot be more than 20 characters!")]
        public string FaxNo { get; set; }

        /// <summary>
        /// Get/Set C/o
        /// </summary>
        [StringLength(50, ErrorMessage = "C/o cannot be more than 50 characters!")]
        public string CO { get; set; }

        /// <summary>
        /// Get/set Street address
        /// </summary>
        [StringLength(250, ErrorMessage = "Street Address cannot be more than 250 characters!")]
        public string StreetAddress { get; set; }

        /// <summary>
        /// Get/Set Town
        /// </summary>
        [StringLength(50, ErrorMessage = "Town cannot be more than 50 characters!")]
        public string Town { get; set; }

        /// <summary>
        /// Get/Set State
        /// </summary>
        [StringLength(30, ErrorMessage = "State cannot be more than 30 characters!")]
        public string State { get; set; }

        /// <summary>
        /// Get/Set State
        /// </summary>
        [StringLength(10, ErrorMessage = "post code cannot be more than 10 characters!")]
        public string Postcode { get; set; }

        /// <summary>
        /// Get/Set Country
        /// </summary>
        [StringLength(150, ErrorMessage = "Country cannot be more than 150 characters!")]
        public string Country { get; set; }

        /// <summary>
        /// Get/Set postal address
        /// </summary>
        [StringLength(250, ErrorMessage = "postal address cannot be more than 250 characters!")]
        public string PostalAddress { get; set; }

        /// <summary>
        /// Get/Set Shareholder Login
        /// </summary>
        public ShareholderLoginModel ShareholderLogin { get; set; }

    }

    /// <summary>
    /// Shareholder login model
    /// </summary>
    [Compare("Password", "ReTypePassword", ErrorMessage = "Password and Retyped password must match")]
    public class ShareholderLoginModel : ModelWithValidation
    {
        /// <summary>
        /// Empty Constructor
        /// </summary>
        public ShareholderLoginModel() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DB">Data Context</param>
        /// <param name="Id">Login Id</param>
        public ShareholderLoginModel(ShareHolderDataContext DB, Guid Id)
        {
            if (DB != null && Id != Guid.Empty)
            {
                Shareholder_Login shareholderLogin = DB.Shareholder_Logins.FirstOrDefault(shl => shl.ID == Id);

                if (shareholderLogin != null)
                {
                    this.ID = shareholderLogin.ID;
                    this.AccountLockedUntil = shareholderLogin.AccountLockedUntil;
                    this.Created = shareholderLogin.Created;
                    this.CreatedByID = shareholderLogin.CreatedByID;
                    this.Deleted = shareholderLogin.Deleted;
                    this.DeletedDate = shareholderLogin.DeletedDate;
                    this.EmailAddress = shareholderLogin.EmailAddress;
                    this.FailedLoginCount = shareholderLogin.FailedLoginCount;
                    this.IsAdmin = shareholderLogin.IsAdmin;
                    this.LastLoginDate = shareholderLogin.LastLoginDate;
                    this.LostPasswordAnswer = shareholderLogin.LostPasswordAnswer;
                    this.LostPasswordQuestion = shareholderLogin.LostPasswordQuestion;
                    this.ReceiveNewsletter = shareholderLogin.ReceiveNewsletter;
                    this.Updated = shareholderLogin.Updated;
                    this.UserName = shareholderLogin.UserName;
                }
            }

        }

        /// <summary>
        /// Creates Shareholder login model
        /// </summary>
        /// <param name="shareholderLogin">Shareholder_login table data</param>
        public ShareholderLoginModel(Shareholder_Login shareholderLogin = null)
        {
            if (shareholderLogin != null)
            {
                this.ID = shareholderLogin.ID;
                this.AccountLockedUntil = shareholderLogin.AccountLockedUntil;
                this.Created = shareholderLogin.Created;
                this.CreatedByID = shareholderLogin.CreatedByID;
                this.Deleted = shareholderLogin.Deleted;
                this.DeletedDate = shareholderLogin.DeletedDate;
                this.EmailAddress = shareholderLogin.EmailAddress;
                this.FailedLoginCount = shareholderLogin.FailedLoginCount;
                this.IsAdmin = shareholderLogin.IsAdmin;
                this.LastLoginDate = shareholderLogin.LastLoginDate;
                this.LostPasswordAnswer = shareholderLogin.LostPasswordAnswer;
                this.LostPasswordQuestion = shareholderLogin.LostPasswordQuestion;
                this.ReceiveNewsletter = shareholderLogin.ReceiveNewsletter;
                this.Updated = shareholderLogin.Updated;
                this.UserName = shareholderLogin.UserName;
            }
        }

        /// <summary>
        /// Returns Database model Shareholder_Login
        /// </summary>
        /// <returns></returns>
        public Shareholder_Login GetDatabaseModel(Shareholder_Login shareholderLogin = null)
        {
            if (shareholderLogin == null)
                shareholderLogin = new Shareholder_Login();

            shareholderLogin.ID = this.ID;
            shareholderLogin.UserName = this.UserName;
            shareholderLogin.EmailAddress = this.EmailAddress;
            shareholderLogin.IsAdmin = this.IsAdmin;
            shareholderLogin.LostPasswordAnswer = this.LostPasswordAnswer;
            shareholderLogin.LostPasswordQuestion = this.LostPasswordQuestion;
            shareholderLogin.ReceiveNewsletter = this.ReceiveNewsletter;
            return shareholderLogin;
        }

        /// <summary>
        /// Get/Set ID
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// Get/Set User  name
        /// </summary>
        [StringLength(100, ErrorMessage = "User name cannot be more than 100 characters!")]
        [Required(ErrorMessage = "User  name cannot be empty!")]
        public string UserName { get; set; }

        /// <summary>
        /// Get/set password
        /// </summary>
        [Required(ErrorMessage = "Password  cannot be empty")]
        public string Password { get; set; }

        /// <summary>
        /// Get/set Re-type password
        /// </summary>
        public string ReTypePassword { get; set; }

        /// <summary>
        /// Get/Set Email Address
        /// </summary>
        [Required(ErrorMessage = "Email Address cannot be empty")]
        [RegularExpression(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", ErrorMessage = "Email address is not valid")]
        [StringLength(150, ErrorMessage = "User name cannot be more than 150 characters!")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// Get/Set lost password question
        /// </summary>
        [StringLength(250, ErrorMessage = "Lost Password Question cannot be more than 250 characters")]
        [Required(ErrorMessage = "Lost password question cannot be empty")]
        public string LostPasswordQuestion { get; set; }

        /// <summary>
        /// Get/set Lost password answer
        /// </summary>
        [StringLength(250, ErrorMessage = "Lost Password answer cannot be more than 250 characters")]
        [Required(ErrorMessage = "Lost password answer cannot be empty")]
        public string LostPasswordAnswer { get; set; }

        /// <summary>
        /// Get/Set Admin flag
        /// </summary>
        public bool IsAdmin { get; set; }

        /// <summary>
        /// Get/set Receive Newsletter flag
        /// </summary>
        public bool ReceiveNewsletter { get; set; }

        /// <summary>
        /// Get/Set Failed Login count
        /// </summary>
        public int? FailedLoginCount { get; set; }

        /// <summary>
        /// Get/Set Account locked  until date and time
        /// </summary>
        public DateTime? AccountLockedUntil { get; set; }

        /// <summary>
        /// Get/set Last Login date
        /// </summary>
        public DateTime? LastLoginDate { get; set; }

        /// <summary>
        /// Get/Set Created Date
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        /// Get/set created by Id
        /// </summary>
        public Guid CreatedByID { get; set; }

        /// <summary>
        /// Get/set Updated date time
        /// </summary>
        public DateTime? Updated { get; set; }

        /// <summary>
        /// Get/Set Deleted flag
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Get/set deleted date
        /// </summary>
        public DateTime? DeletedDate { get; set; }

    }
}