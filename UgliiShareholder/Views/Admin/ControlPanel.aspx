﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	ControlPanel
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>ControlPanel</h2>
        <ul>
            <li><%: Html.ActionLink("Manage Shareholders", "Index", "Shareholder")%></li>

            <li><%: Html.ActionLink("Manage Users", "Index", "ShareholderLogin")%></li>
            
            <li><%: Html.ActionLink("Manage Newsletters", "Index", "NewsLetter")%></li>
            
            <li><%: Html.ActionLink("System Notifications", "Index", "Home")%></li>

            <li><%: Html.ActionLink("Manage Site Text", "Index", "Home")%></li>

            <li><%: Html.ActionLink("Manage Downloadable Forms", "Index", "Home")%></li>

            <li><%: Html.ActionLink("Manage Share Price for Evaluation", "Index", "Home")%></li>
        </ul>
</asp:Content>
