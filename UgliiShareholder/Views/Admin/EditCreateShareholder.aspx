﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <ul class="tabs">
                <li data-bind="css: { current: _CurrentTab() == 'details' }"><a href="#details" data-bind="    click: function () { _SetTab('details') }">Details</a></li>
            </ul>

            <div class="tabs-content">

                <div id="details" data-bind="visible: _CurrentTab() == 'details'">

                    <div class="twoColumns">
                        <label class="required">Shareholder Name</label>
                        <input type="text" data-bind="attr: { value: ShareholderName, disabled: _Mode() == 2 ? 'disabled' : null }" id="ShareholderName" maxlength="150" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">Beneficiary</label>
                        <input type="text" data-bind="value: Beneficiary" maxlength="150" id="Beneficiary" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Mobile Phone</label>
                        <input type="text" data-bind="value: MobilePhone" maxlength="20" id="MobilePhone" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Fixed Line Phone</label>
                        <input type="text" data-bind="value: FixedLinePhone" maxlength="20" id="FixedLinePhone" />
                    </div>

                    <div class="twoColumns">
                        <label class="required">
                            Alternate Phone</label>
                        <input type="text" data-bind="value: AlternatePhone" maxlength="20" id="AlternatePhone" />
                    </div>
                    <div class="twoColumns">
                        <label>
                            Fax No</label>
                        <input type="text" data-bind="value: FaxNo" maxlength="20" id="FaxNo" />
                    </div>
                    <div class="twoColumns">
                        <label>C/o</label>
                        <input type="text" data-bind="value: CO" maxlength="50" id="CO" />
                    </div>
                    <div class="twoColumns">
                        <label>Street Address</label>
                        <input type="text" data-bind="value: StreetAddress" maxlength="250" id="StreetAddress" />
                    </div>
                    <div class="twoColumns">
                        <label>Town</label>
                        <input type="text" data-bind="value: Town" maxlength="50" id="Town" />
                    </div>
                    <div class="twoColumns">
                        <label>State</label>
                        <input type="text" data-bind="value: State" maxlength="30" id="State" />
                    </div>
                    <div class="twoColumns">
                        <label>Postcode</label>
                        <input type="text" data-bind="value: Postcode" maxlength="10" id="Postcode" />
                    </div>
                    <div class="twoColumns">
                        <label>Country</label>
                        <input type="text" data-bind="value: Country" maxlength="150" id="Country" />
                    </div>
                    <div class="twoColumns">
                        <label>Postal Address</label>
                        <input type="text" data-bind="value: PostalAddress" maxlength="250" id="PostalAddress" />
                    </div>
                </div>
            </div>

        </div>
    </fieldset>

</asp:Content>
