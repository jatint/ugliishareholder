﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <div class="tabs-content">
                <div id="LoginDetails">
                    <div class="oneColumn">
                        Delete Shareholder Login <%= ViewData["name"] != null  ? ViewData["name"].ToString() : ""  %> 
                    </div>
                    <div class=""></div>
                    <div class="oneColumn">
                       <table style="width:100%">
                            <thead>
                                <tr>
                                    <th>Shareholder name</th>
                                    <th>Beneficiary</th>
                                    <th>MobilePhone</th>
                                    <th>Town</th>
                                    <th>Country</th>
                                </tr>
                            </thead>
                            <tbody data-bind="attr: { style: Id == '00000000-0000-0000-0000-000000000000' ? 'display:none' : null }">
                                <tr>
                                    <td data-bind="text: ShareholderName"></td>
                                    <td data-bind="text: Beneficiary"></td>
                                    <td data-bind="text: MobilePhone"></td>
                                    <td data-bind="text: Town"></td>
                                    <td data-bind="text: Country"></td>
                                </tr>
                            </tbody>
                            <tbody data-bind="attr: { style: Id != '00000000-0000-0000-0000-000000000000' ? 'display:none' : null }">
                                <tr>
                                    <td colspan="4" >No User Info</td>
                                </tr>
                            </tbody>

                        </table>k
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

</asp:Content>
