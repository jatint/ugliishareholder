﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/StandardListUgliiGrid.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phHead" runat="server">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="phTitle" runat="server">
    ShareHolder Login Management
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="cphMainPage" runat="server">

    <style type="text/css">
        ul.controls-buttons li a.help {
            margin-left: 8px;
        }

            ul.controls-buttons li a.help img {
                vertical-align: middle;
                margin-top: -6px;
            }

        #grid {
            overflow: auto;
        }

        span.mandatory {
            float: left;
            margin-top: 10px;
            font-size: 18px;
            margin-left: 2px;
        }

        .modal-window{
            background-color:white;
            padding:10px;
        }
    </style>

    <h1>Manage Shareholder Login</h1>

    <div class="task">
        <div class="block-controls">
            <div style="display: none;" id="GridMessage" class="message success">Data was Succesfully Saved</div>
            <% Html.RenderPartial("UgliiGridPaging", "ddPage"); %>
            <ul class="controls-buttons">
                <li><a href="#" id="addMarketingLink" class="add small" onclick="javascript: new ShareholderLogin(null, 0)._ShowInsertEdit(null, 0)">Register New User</a></li>
            </ul>
        </div>
        <div id="grid" style="display: none;">
            <table class="table" width="100%">
                <thead>
                    <tr>
                        <th class="sorting filtering"></th>

                        <th class="sorting filtering">{column:UserName}<span class="UGColName">User Name</span></th>

                        <th class="sorting filtering">{column:EmailAddress}<span class="UGColName">Email Address</span></th>

                        <th class="sorting filtering">{column:IsAdmin}<span class="UGColName">Admin</span></th>

                        <th class="sorting filtering">{column:ReceiveNewsletter}<span class="UGColName">Receive Newsletter</span></th>

                        <th class="sorting filtering">{column:Created}<span class="UGColName">Created</span></th>
                        
                    </tr>
                </thead>
                <tbody id="hasLinks">
                    <tr>
                        <td>
                            <div class="button menu-opener medium-margin">
                                Action
                                <div class="menu-arrow">
                                    <img height="16" width="16" src="{CONFIG:CONSTELLATIONPATH}/images/menu-open-arrow.png" />
                                </div>
                                <div class="menu">
                                    <ul style="opacity: 1;">
                                        <li class="icon_edit">
                                            <a href="#" onclick="new ShareholderLogin(null)._ShowInsertEdit('{field:ID}', 1)" title="Edit" class="with-tip">Edit</a>
                                        </li>
                                        <li class="icon_search">
                                            <a href="#" onclick="new ShareholderLogin(null)._ShowInsertEdit('{field:ID}', 2)" title="Edit" class="with-tip">View</a>
                                        </li>
                                        <li class="icon_delete">
                                            <a href="#" onclick="new ShareholderLogin(null)._DeleteData('{field:ID}','{field:UserName}')" title="Delete" class="with-tip">Delete</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </td>

                        <td style="word-break: break-all!important;">{field:UserName}
                        </td>
                        <td style="word-break: break-all!important;">{field:EmailAddress}
                        </td>
                        <td style="word-break: break-all!important;">{field:IsAdmin}
                        </td>
                        <td style="word-break: break-all!important;">{field:ReceiveNewsletter}
                        </td>
                        <td style="word-break: break-all!important;">{field:Created}
                        </td>
                    </tr>
                </tbody>
                <tfoot></tfoot>
            </table>
        </div>
    </div>

    <script type="text/javascript" src="../Scripts/ControllersJS/ShareholderLoginManagement.js"></script>
</asp:Content>

