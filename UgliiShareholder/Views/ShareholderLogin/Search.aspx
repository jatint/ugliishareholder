﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

             div.oneColumn input{
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <div id="error"></div>
            <div class="oneColumn">
                <label class="required">Search by</label>
                <select id="searchtype">
                    <option value="1">User Name</option>
                    <option value="2">Email</option>
                </select>
            </div>

            <div class="oneColumn">
                <label class="required">Search Keyword</label>
                <input type="text" maxlength="150" id="keyword" />
            </div>
            <div class="oneColumn">
                <button id="search" data-bind="click: _SearchUser">Search</button>
            </div>
            <div data-bind="visible: _SearchResult()">
                <table style="width:100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>User Name</th>
                            <th>Email Address</th>
                            <th>Admin</th>
                            <th>Receive Newsletter</th>

                        </tr>
                    </thead>
                    <tbody data-bind="foreach: _SearchResult">
                        <tr>
                            <td><input name="result" type="radio" data-bind="checked: _IsSelected" /></td>
                            <td data-bind="text: UserName"></td>
                            <td data-bind="text: EmailAddress"></td>
                            <td data-bind="text: IsAdmin"></td>
                            <td data-bind="text: ReceiveNewsletter"></td>
                        </tr>
                    </tbody>
                    <tbody data-bind="if: _DisplayMessage">
                        <tr>
                            <td colspan="5">No Result Found</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

    </fieldset>

</asp:Content>
