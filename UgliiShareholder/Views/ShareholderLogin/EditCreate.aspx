﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/StandardPopupForKnockout.Master" %>


<asp:Content ID="headContent" ContentPlaceHolderID="phHeader" runat="server">
</asp:Content>

<asp:Content ID="phContent" ContentPlaceHolderID="phMainPage" runat="server">
    <style>
        /*=========================================*/

        .form input[type=text], .form input[type=password], .form .input-type-text {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        .form select {
            padding: 0.385em 0.0em 0.385em 0.3em;
        }

        .form textarea {
            padding: 0.5em 0.0em 0.5em 0.3em;
        }

        div.oneColumn {
            width: 99%;
            vertical-align: top;
        }

            div.oneColumn select {
                width: 99%;
            }

        div.twoColumns {
            vertical-align: top;
            display: inline-block;
            width: 49%;
        }

            div.twoColumns select {
                width: 99%;
            }

            div.twoColumns input {
                width: 99%;
            }

            div.twoColumns textarea {
                width: 99%;
                height: 100px;
            }

        div.oneColumn textarea {
            width: 99%;
            height: 100px;
        }

        #mce_8-body, #mce_32-body, #mce_56-body, #mce_80-body, #mce_104-body, #mce_128-body {
            float: right;
        }

        .mce-btn button {
            padding: 2px 4px !important;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.4);
        }
    </style>
    <fieldset>
        <div class="form">
            <ul class="tabs" style="display: none">
                <li data-bind="css: { current: _CurrentTab() == 'LoginDetails' }"><a href="#LoginDetails" data-bind="    click: function () { _SetTab('LoginDetails') }">Login Details</a></li>
            </ul>

            <div class="tabs-content">
                <div id="LoginDetails" data-bind="visible: _CurrentTab() == 'LoginDetails'">
                    <div class="twoColumns">
                        <label>User Name</label>
                        <input type="text" data-bind="value: UserName, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="100" id="UserName" />
                    </div>
                    <div class="twoColumns" data-bind="attr: { style: _Mode() != 0 ? 'display:none;' : null }">
                        <label>Password</label>
                        <input type="password" data-bind="value: Password" maxlength="100" id="Password" />
                    </div>
                    <div class="twoColumns" data-bind="attr: { style: _Mode() != 0 ? 'display:none;' : null }">
                        <label>Re-Type Password</label>
                        <input type="password" data-bind="value: ReTypePassword" maxlength="100" id="ReTypePassword" />
                    </div>
                    <div class="twoColumns">
                        <label>Email Address</label>
                        <input type="text" data-bind="value: EmailAddress, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="150" id="EmailAddress" />
                    </div>
                    <div class="twoColumns">
                        <label>Lost Password Question</label>
                        <input type="text" data-bind="value: LostPasswordQuestion, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="250" id="LostPasswordQuestion" />
                    </div>
                    <div class="twoColumns">
                        <label>Lost Password Answer</label>
                        <input type="text" data-bind="value: LostPasswordAnswer, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" maxlength="250" id="LostPasswordAnswer" />
                    </div>

                    <div class="twoColumns">
                        <label>Admin</label>
                        <input type="checkbox" data-bind="checked: IsAdmin, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" />
                    </div>
                    <div class="twoColumns">
                        <label>Receive Newsletter</label>
                        <input type="checkbox" data-bind="checked: ReceiveNewsletter, attr: { disabled: _Mode() == 2 ? 'disabled' : null }" />
                    </div>
                </div>
            </div>
        </div>
    </fieldset>

</asp:Content>
