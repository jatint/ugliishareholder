﻿using System;
using System.Linq;

namespace UgliiShareholder.DataModel
{

    public partial class ShareHolderDataContext
    {
        public Guid CurrentUserId { get; set; }
    }

    public partial class UserSession
    {
        public UserSession(ShareHolderDataContext DB)
            : this()
        {
            var theObj = ((UserSession)this);
            theObj.ID = Guid.NewGuid();
            theObj.Created = DateTime.UtcNow;
            theObj.Updated = DateTime.UtcNow;
            theObj.UpdatedByID = DB.CurrentUserId;
            theObj.CreatedByID = DB.CurrentUserId;
            IsWebApplication = true;
            theObj.Deleted = false;
        }

    }

    public partial class Config
    {
        
        /// <summary>
        /// Note, this caches responses for 1 hour, so subsequent calls should have minimal DB impact.
        /// </summary>
        /// <param name="db"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetValueByKey(ShareHolderDataContext db, string key)
        {
            System.Web.Caching.Cache app = System.Web.HttpContext.Current.Cache;
            Config c = null;
            if (app["CONFIG_" + key.ToLower()] == null)
            {
                c = db.Configs.FirstOrDefault(q => q.ConfigKey == key && !(q.Deleted ?? false));
                if (c != null)
                {  //Only add to cache if not null.
                    app.Add("CONFIG_" + key.ToLower(), c, null, System.Web.Caching.Cache.NoAbsoluteExpiration, new TimeSpan(1, 0, 0), System.Web.Caching.CacheItemPriority.Normal, null);
                }
                else
                {
                    throw new Exception("Config value: " + key + " could not be found");
                }

            }
            else
            {
                c = (Config)app["CONFIG_" + key.ToLower()];
            }
            return c == null ? null : c.ConfigValue;

        }
    }

    public partial class Shareholder_Detail
    {
        public static Guid SYSADMINID = Guid.Parse("0D0A6D5C-A34D-4D39-A8D9-813BCDC51E64");

    }


}