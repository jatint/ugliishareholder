﻿using log4net;
namespace UgliiShareholder.DataModel
{

    public partial class ShareHolderDataContext : System.Data.Linq.DataContext
    {
        public ShareHolderDataContext() :
            base(global::System.Configuration.ConfigurationManager.ConnectionStrings["UgliiShareholder_DevConnectionString"].ConnectionString, mappingSource)
        {
            OnCreated();
        }
    }

    /// <summary>
    /// This class is used to create a connection to the Link to SQL and provide common connection functionality. This should be
    /// used instead of calling <see cref="LocatorServiceDataContext"/>
    /// </summary>
    public class ContextFactory
    {

        public static readonly ILog LogManager = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get a new context to the EntityFramework using the connection specified in the application configuration file
        /// </summary>
        /// <returns>A new entity framework context for Uglii Find</returns>
        public static ShareHolderDataContext GetEntityContext()
        {

            LogManager.Debug("[GetEntityContext] - Creating a connection to the database");

            var context = new ShareHolderDataContext();
            LogManager.Debug("[GetEntityContext] - DB ConnectionString = " + context.Connection.ConnectionString);
            LogManager.Debug(string.Format("[GetEntityContext] - Connected to the database '{0}' on server {1}", context.Connection.Database, context.Connection.DataSource));

            return context;
        }

        
    }
}
