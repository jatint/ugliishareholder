﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UgliiShareholder.Core
{

    /// <summary>
    /// Inherit this for Models that are sent to the client. This allows the validation error in the ViewModel to be transmitted to 
    /// the client (preferable in knockout) for display.
    /// </summary>
    public class ModelWithValidation
    {
        public IList<ValidationItemModel> ValidationErrors { get; set; }

        /// <summary>
        /// Populates ValidationErrors with all the errors in the validation model so they can be presented to the client via the json model.
        /// </summary>
        /// <param name="ModelState"></param>
        public void GetValidationErrorsFromViewModel(ModelStateDictionary ModelState)
        {
            ValidationErrors = ModelState.SelectMany(s => s.Value.Errors, (sa, sb) => new ValidationItemModel() { Key = sa.Key, ErrorMessage = sb.ErrorMessage == "" ? sb.Exception.Message : sb.ErrorMessage }).Where(q2 => q2.ErrorMessage != "").ToList();

        }
    }

    public class ValidationItemModel
    {
        public string Key { get; set; }
        public string ErrorMessage { get; set; }
        public string Context { get; set; }
    }
}