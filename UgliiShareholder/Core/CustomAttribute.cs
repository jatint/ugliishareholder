﻿using Resources;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace UgliiShareholder.Core.Attributes
{
    public class CompareAttribute : ValidationAttribute
    {
        public CompareAttribute(string property, string propertyToCompare)
            : base(MvcResources.CompareAttribute_MustMatch)
        {
            if (string.IsNullOrEmpty(property))
            {
                throw new ArgumentNullException("property");
            }

            if (string.IsNullOrEmpty(propertyToCompare))
            {
                throw new ArgumentNullException("PropertyToCompare");
            }

            Property = property;
            PropertyToCompare = propertyToCompare;
        }

        public string Property { get; private set; }

        public string PropertyToCompare { get; private set; }

        public string OtherPropertyDisplayName { get; internal set; }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture, ErrorMessageString, name, OtherPropertyDisplayName ?? Property);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return null;
            }
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
            var property = properties.Find(Property, true);
            var propertyToCompare = properties.Find(PropertyToCompare, true);

            if (property == null)
            {
                return new ValidationResult(String.Format(CultureInfo.CurrentCulture, MvcResources.CompareAttribute_UnknownProperty, Property));
            }
            if (propertyToCompare == null)
            {
                return new ValidationResult(String.Format(CultureInfo.CurrentCulture, MvcResources.CompareAttribute_UnknownProperty, propertyToCompare));
            }

            var PropertyValue = (property.GetValue(value) ?? "").ToString();
            var PropertyCompareValue = (propertyToCompare.GetValue(value) ?? "").ToString();
            var isValid = PropertyCompareValue == PropertyValue;

            if (!isValid)
            {
                return new ValidationResult(ErrorMessage, new List<string> { Property });
            }
            return null;
        }
    }
}