﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using System.Configuration;
using log4net.Config;
using System.Web;
//using Microsoft.Practices.ServiceLocation;
using log4net.Core;
using UgliiShareholder.DataModel;
using hdm = UgliiShareholder.DataModel;

namespace UgliiShareholder.Core
{
  /* Generic class for mvc application to override, which exposes a logger using the log4net */
  public class Application : System.Web.HttpApplication
  {
      public static ILog LogManager =   log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    protected void Application_Error(object sender, EventArgs e)
    {
        Exception exception = Server.GetLastError();
        if (exception is HttpException)
        {
            HttpException httpException = exception as HttpException;
            string msg = string.Format("Error Code: {0}{2} -Url: {1}{2} -Referer: {3}{2} -Html Message: {4}", httpException.GetHttpCode(), HttpContext.Current.Request.Url,
                                       Environment.NewLine, HttpContext.Current.Request.UrlReferrer, httpException.GetHtmlErrorMessage());
            //Log the Request params too.
            StringBuilder sb = new StringBuilder();
            if (Request != null)
            {
                for (int i = 0; i < Request.Params.Count; i++)
                {
                    sb.AppendFormat("{0}={1}\n", Request.Params.Keys[i], Request.Params[i]);
                }
            }
            LogManager.Error(msg + "\n" + sb.ToString(), exception);
        }
        else
        {
            //Log the Request params too.
            StringBuilder sb = new StringBuilder();
            if (Request != null)
            {
                for (int i = 0; i < Request.Params.Count; i++)
                {
                    sb.AppendFormat("{0}={1}\n", Request.Params.Keys[i], Request.Params[i]);
                }
            }
            LogManager.ErrorFormat(exception.StackTrace + "\n" + sb.ToString(), exception);
        }
        if (Request.AcceptTypes!=null && Request.AcceptTypes.Contains("json"))
        {
            Response.ContentType = "application/json";
            Response.Write("{\"Success\":\"false\", \"Error\":\"" + exception.Message + "\" }");
            Response.End();
        }
        else
        {
            Response.Clear();
            var errorMessage = "";
            ShareHolderDataContext db = new ShareHolderDataContext();
            string varApplicationEnvironment = hdm.Config.GetValueByKey(db,"ApplicationEnvironment");
            if (varApplicationEnvironment.ToLower() == "production")
            {
                errorMessage = GenerateGenericError(); // for users
            }
            else
            {
                errorMessage = GenerateErrorForDevelopment(exception); // for developers
            }
            Response.Write(errorMessage);
            if (System.Web.HttpContext.Current.Session != null)
            {
                Session["ERROR"] = errorMessage;
            }
            Response.End();
        }
    }

    private string GenerateErrorForDevelopment(Exception ex)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
        sb.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        sb.Append("<head>\n");
        sb.Append("<link href=\"/Content/Error.css\" rel=\"stylesheet\" type=\"text/css\" />\n");
        sb.Append("</head>\n");
        sb.Append("<body>\n");
        sb.Append("<h1>Application Error</h1>\n");
        sb.Append("<table class=\"error\" cellspacing=\"0\" cellpadding=\"0\">");
        while (ex != null)
        {
            sb.Append("<tr><td colspan=\"2\" class=\"errorhead\"/>Exception</td></tr>");
            sb.Append("<tr><td class=\"errortitle\">Message</td><td>" + ex.Message + "</td></tr>");
            sb.Append("<tr><td class=\"errortitle\">Source</td><td>" + ex.Source + "</td></tr>");
            if (ex.StackTrace != null)
            {
                sb.Append("<tr><td class=\"errortitle\">StackTrace</td><td>" + ex.StackTrace.Replace("at ", "<br>at ") + "</td></tr>");
            }
            ex = ex.InnerException;
        }
        sb.Append("<tr><td colspan=\"2\" class=\"errorhead\"/>Request</td></tr>");
        if (Request != null)
        {
            for (int i = 0; i < Request.Params.Count; i++)
            {
                sb.Append("<tr><td  class=\"errortitle\">" + Request.Params.Keys[i] + "</td><td>" + Request.Params[i] + "</td></tr>");
            }
        }
        sb.Append("</table>");
        sb.Append("</html>");

        return (sb.ToString());
    }

    private string GenerateGenericError()
    {
        hdm.ShareHolderDataContext DB = new hdm.ShareHolderDataContext();
        var constellationPath = hdm.Config.GetValueByKey(DB, "ConstellationPath");

        StringBuilder sb = new StringBuilder();
        DateTime dt = DateTime.UtcNow;

        sb.Append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n");
        sb.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
        sb.Append("<head>\n");
        sb.Append("<title>An error has occurred | Uglii</title>\n");
        sb.Append("<link href=\"" + constellationPath + "/css/reset.css\" rel=\"stylesheet\" type=\"text/css\">\n");
        sb.Append("<link href=\"" + constellationPath + "/css/common.css\" rel=\"stylesheet\" type=\"text/css\">\n");
        sb.Append("<link href=\"" + constellationPath + "/css/standard.css\" rel=\"stylesheet\" type=\"text/css\">\n");
        sb.Append("</head>\n");
        sb.Append("<body>\n");
        sb.Append("<div class=\"block-border\" style=\"margin:4em auto; width:600px;\"><div class=\"block-content\">");
        sb.Append("<div class=\"content\">");
        sb.Append("<p>" + String.Format("{0:dd/MM/yyyy HH:mm:ss}", dt) + "</p>");
        sb.Append("<p>An error has occurred.</p>");
        sb.Append("<p>This has been logged with technical support and will be rectified as soon as possible.</p>");
        sb.Append("<p>Thank you for your patience.</p>");
        sb.Append("<input type=\"button\" value=\"Back\" onclick=\"history.back();\">");
        sb.Append("</div>");
        sb.Append("</div>");
        sb.Append("<div class=\"block-content no-title grey-bg\"><div class=\"float-right\">");
        sb.Append("<img src=\"" + constellationPath + "/images/common/uglii_logo_black_footer.png\" />");
        sb.Append("</div><div class=\"clear\"></div></div></div>");
        sb.Append("</body>");
        sb.Append("</html>");

        return sb.ToString();
    }

    public override void Init()
    {
        base.Init();

    }

  }
}
