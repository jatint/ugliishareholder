﻿var mygrid;
var emptyGUID = '00000000-0000-0000-0000-000000000000';
var ko = null;

$(document).ready(function () {
    mygrid = new UgliiGrid();
    mygrid.SortBy = "operationkey";
    mygrid.IndexURL = "/SecuredOperations/Index";
    mygrid.InitGrid();

    ko.o = ko.observable;
    ko.oA = ko.observableArray;
    ko.c = ko.computed;

});

var PageModel = function () {
    var self = this;
    self.CurrentItem = ko.o();
}


function SecuredOperations(data) {
    var self = this;
    self.ValidationErrors = ko.oA([]);
    self._CurrentTab = ko.o("details");
    self._SetTab = function (val) {
        self._CurrentTab(val);
    }
    self.Id = data && data.ID || emptyGUID;
    self.OperationKey = ko.o(data && data.OperationKey || '');
    self.Description = ko.o(data && data.Description || '');
    self.IsAdmin = ko.o(data && data.IsAdmin || false);
    self.Deleted = ko.o(data && data.Deleted || false);
   
    self._InsertEditModal = null;
    self._ShowInsertEdit = function (Id) {
        var title = Id ? 'Insert' : 'Edit';
        self._InsertEditModal = $.modal({
            url: "/SecuredOperations/EditCreate",
            type: 'POST',
            title: title,
            resizable: true,
            border: true,
            draggable: true,
            width: 900,
            complete: function () {
                thePage = new PageModel();
                if (Id) {
                    $.ajax({
                        url: "/SecuredOperations/GetData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            self._InsertEditModal.setModalContent(data);
                            self._InsertEditModal.unblock();
                        }
                    }).success(function (data) {
                        var item = new SecuredOperations(data);
                        thePage.CurrentItem(item);
                        ko.applyBindings(thePage, self._InsertEditModal[0]);
                        self._InsertEditModal.unblock();
                    });
                } else {
                    var item = new SecuredOperations(null);
                    thePage.CurrentItem(item);
                    ko.applyBindings(thePage, self._InsertEditModal[0]);
                    self._InsertEditModal.unblock();
                }
            },
            buttons: {
                "Save": function () {
                    self._SaveData();
                },

                "Close": function () {
                    self._InsertEditModal.closeModal();
                }
            }
        });
        self._InsertEditModal.block({ message: 'loading...' });
    }
    self._ScrollToTop = function () {
        $(".modal-content").animate({ scrollTop: 0 }, 'fast');
    }
    self._BindValidationError = function (serverData, clientData, modalWindow) {
        $(modalWindow).find(".input-validation-error").removeClass("input-validation-error");
        $(modalWindow).find(".tab-validation-error").removeClass("tab-validation-error");
        if (serverData) {
            clientData.ValidationErrors.removeAll();
            if (typeof (serverData.ValidationErrors) == "undefined") {
                //If not a model with ValidationErrors, then put the actual content as the validation message.
                clientData.ValidationErrors.push(new ValidationModel({ ErrorMessage: serverData }));
            } else {
                for (var error in serverData.ValidationErrors) {
                    clientData.ValidationErrors.push(new ValidationModel(serverData.ValidationErrors[error]));
                    var selector = "[id='" + serverData.ValidationErrors[error].Key + "']";
                    $(selector).addClass("input-validation-error");
                }
            }
            //This gets the grandparent of any controls with input-validation-error, and then looks for a tab with the same id, and adds the tab-validation-error into it.
            $(modalWindow).find(".input-validation-error").parents("[id]").each(function () {
                $("a[href='#" + $(this).attr("id") + "']").addClass("tab-validation-error");
            });
        }
    }
    self._SaveData = function () {
        $(".modal-window").select();
        self._InsertEditModal.block({ message: 'Saving....' });
        var item = thePage.CurrentItem();
        var d = ko.toJSON(item, function (key, value) {
            if (isNaN(key)) {
                if (key.substring(0, 1) == "_") {
                    return;
                } else {
                    return value;
                }
            }
            else {
                return value;
            }
        });
        $.ajax({
            url: "/SecuredOperations/SaveData"
            , type: "POST"
            , datatype: "json"
            , contentType: "application/json charset=utf-8"
            , data: d
        }).success(function (result) {
            if (typeof (result.success) != "undefined" && result.success) {
                if (typeof (mygrid) != "undefined") {
                    mygrid.RefreshGrid(function () {
                        self._InsertEditModal.closeModal();
                        $("#GridMessage").text("Data Saved");
                        $("#GridMessage").show("fast");
                        var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                    });
                } else {
                    self._InsertEditModal.closeModal();
                }
            } else {
                self._BindValidationError(result.data, thePage.CurrentItem(), self._InsertEditModal);
                self._ScrollToTop();
                self._InsertEditModal.unblock();
            }
        }).error(function (data) {
            self._InsertEditModal.setModalContent(data);
        });
    };
}


