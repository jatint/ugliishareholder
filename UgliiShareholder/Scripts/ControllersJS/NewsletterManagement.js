﻿var mygrid;
var emptyGUID = '00000000-0000-0000-0000-000000000000';
var ko = null;
var shareholderPage;
var shareholderLoginPage;
var searchPage;
var InsertEditLoginModal;
var _InsertEditModal;
var SearchUserModal;
var deleteModal;
$(document).ready(function () {
    mygrid = new UgliiGrid();
    mygrid.SortBy = "";
    mygrid.IndexURL = "/Shareholder/Index";
    mygrid.InitGrid();

    ko.o = ko.observable;
    ko.oA = ko.observableArray;
    ko.c = ko.computed;

});

var PageModel = function () {
    var self = this;
    self.CurrentItem = ko.o();
}

function ShareholderDetails(data) {
    var self = this;
    self.ValidationErrors = ko.oA([]);
    self._CurrentTab = ko.o("details");
    self._SetTab = function (val) {
        self._CurrentTab(val);
    }
    self._Mode = ko.o(0);
    self.Id = data && data.Id || emptyGUID;
    self.ShareholderName = ko.o(data && data.ShareholderName || '');
    self.Beneficiary = ko.o(data && data.Beneficiary || '');
    self.MobilePhone = ko.o(data && data.MobilePhone || '');
    self.FixedLinePhone = ko.o(data && data.FixedLinePhone || '');
    self.AlternatePhone = ko.o(data && data.AlternatePhone || '');
    self.FaxNo = ko.o(data && data.FaxNo || '');
    self.CO = ko.o(data && data.CO || '');
    self.StreetAddress = ko.o(data && data.StreetAddress || '');
    self.Town = ko.o(data && data.StreetAddress || '');
    self.State = ko.o(data && data.State || '');
    self.Postcode = ko.o(data && data.Postcode || '');
    self.Country = ko.o(data && data.Country || '');
    self.PostalAddress = ko.o(data && data.PostalAddress || '');
    self.ShareholderLogin = ko.o(new ShareholderLogin(data));
    self._InsertEditModal = null;
    self._ShowInsertEdit = function (Id, mode) { //mode=0  Insert, 1= Edit, 2= View
        var title = mode == 0 ? 'Insert' : mode == 1 ? 'Edit' : 'View';
        _InsertEditModal = $.modal({
            url: "/Shareholder/EditCreate",
            type: 'POST',
            title: title,
            resizable: true,
            border: true,
            draggable: true,
            width: 900,
            complete: function () {
                $('#btnSave').parent().attr('data-bind', 'attr:{style:shareholderPage.CurrentItem()._Mode()==2?\'display:none;\': null}');
                shareholderPage = new PageModel();
                if (Id) {
                    $.ajax({
                        url: "/Shareholder/GetData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            _InsertEditModal.setModalContent(data);
                            _InsertEditModal.unblock();
                        }
                    }).success(function (data) {
                        if (data && typeof (data.success) != 'undefined' && data.success && data.data) {
                            var item = new ShareholderDetails(data.data);
                            if (item.ShareholderLogin()) {
                                item.ShareholderLogin()._Mode(mode);
                            }

                            item._Mode(mode);
                            shareholderPage.CurrentItem(item);
                            ko.applyBindings(shareholderPage, _InsertEditModal[0]);
                            _InsertEditModal.unblock();
                        } else {
                            var item = new ShareholderDetails(null);
                            //item._InsertEditModal = self._InsertEditModal;
                            shareholderPage.CurrentItem(item);
                            ko.applyBindings(shareholderPage, _InsertEditModal[0]);
                            _InsertEditModal.unblock();
                        }
                    });
                } else {
                    var item = new ShareholderDetails(null);
                    //item._InsertEditModal = self._InsertEditModal;
                    shareholderPage.CurrentItem(item);
                    ko.applyBindings(shareholderPage, _InsertEditModal[0]);
                    _InsertEditModal.unblock();
                }
            },
            buttons: {
                "<span id='btnSave' />Save": function () {
                    self._SaveData();
                },

                "Close": function () {
                    _InsertEditModal.closeModal();
                }
            }
        });
        _InsertEditModal.block({ message: 'loading...' });
        //$(_InsertEditModal).center();
    }
    self._ScrollToTop = function () {
        $(".modal-content").animate({ scrollTop: 0 }, 'fast');
    }
    self._BindValidationError = function (serverData, clientData, modalWindow) {
        $(modalWindow).find(".input-validation-error").removeClass("input-validation-error");
        $(modalWindow).find(".tab-validation-error").removeClass("tab-validation-error");
        if (serverData) {
            clientData.ValidationErrors.removeAll();
            if (typeof (serverData.ValidationErrors) == "undefined") {
                //If not a model with ValidationErrors, then put the actual content as the validation message.
                clientData.ValidationErrors.push(new ValidationModel({ ErrorMessage: serverData }));
            } else {
                for (var error in serverData.ValidationErrors) {
                    clientData.ValidationErrors.push(new ValidationModel(serverData.ValidationErrors[error]));
                    var selector = "[id='" + serverData.ValidationErrors[error].Key + "']";
                    $(selector).addClass("input-validation-error");
                }
            }
            //This gets the grandparent of any controls with input-validation-error, and then looks for a tab with the same id, and adds the tab-validation-error into it.
            $(modalWindow).find(".input-validation-error").parents("[id]").each(function () {
                $("a[href='#" + $(this).attr("id") + "']").addClass("tab-validation-error");
            });
        }
    }
    self._SaveData = function () {
        $(".modal-window").select(); //Force blur of any selected items so model updates.
        _InsertEditModal.block({ message: 'Saving....' });
        var item = shareholderPage.CurrentItem();
        var d = ko.toJSON(item, function (key, value) {
            if (isNaN(key)) {
                if (key.substring(0, 1) == "_") {
                    return;
                } else {
                    return value;
                }
            }
            else {
                return value;
            }
        });
        $.ajax({
            url: "/Shareholder/SaveData"
            , type: "POST"
            , datatype: "json"
            , contentType: "application/json charset=utf-8"
            , data: d
        }).success(function (result) {
            if (typeof (result.success) != "undefined" && result.success) {
                if (typeof (mygrid) != "undefined") {
                    mygrid.RefreshGrid(function () {
                        _InsertEditModal.closeModal();
                        $("#GridMessage").text("Data Saved");
                        $("#GridMessage").show("fast");
                        var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                    });
                } else {
                    _InsertEditModal.closeModal();
                }
            } else {
                self._BindValidationError(result.data, shareholderPage.CurrentItem(), _InsertEditModal);
                self._ScrollToTop();
                _InsertEditModal.unblock();
            }
        }).error(function (data) {
            _InsertEditModal.setModalContent(data);
        });
    };
    self._DeleteData = function (Id, name) {
        deleteModal = $.modal({
              content: "<div id=\"warning\"><div class=\"warning-icon\"><div class=\"warning-heading\"><p>Delete Shareholder </p><div class=\"warning-content\">" + name + "</div></div></div></div>"
            , title: 'Delete'
            , width: 450
		    , draggable: true
            , resizable: true
            , buttons: {
                'Delete': function (win) {
                    win.setModalContent('Deleting...', true);
                    deleteModal.block();
                    $.ajax({
                        url: "/Shareholder/DeleteData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            win.setModalContent(data);
                            win.unblock();
                        }
                    }).success(function (data) {
                        if (data.success) {
                            if (typeof (mygrid) != "undefined") {
                                mygrid.RefreshGrid(function () {
                                    _InsertEditModal.closeModal();
                                    $("#GridMessage").text("Shareholder Deleted");
                                    $("#GridMessage").show("fast");
                                    var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                                });
                            } 
                            win.closeModal();
                        } else {
                            win.setModalContent(data.message);
                        }
                        deleteModal.closeModal();
                        win.unblock();
                        
                    });
                }
              , 'Cancel': function (win) {
                  win.unblock();
                  deleteModal.closeModal();
              }
            }
        });

        
        
    }
}

function ShareholderLogin(data) {
    var self = this;
    var Login = data && data.ShareholderLogin || null;
    self.ValidationErrors = ko.oA([]);
    self._CurrentTab = ko.o("LoginDetails");
    self._SetTab = function (val) {
        self._CurrentTab(val);
    }
    self.ID = Login && Login.ID || emptyGUID;
    self._Mode = ko.o(0);
    self.UserName = ko.o(Login && Login.UserName || '');
    self.Password = ko.o(Login && Login.Password || '');
    self.ReTypePassword = ko.o(Login && Login.Password || '');
    self.EmailAddress = ko.o(Login && Login.EmailAddress || '');
    self.LostPasswordQuestion = ko.o(Login && Login.LostPasswordQuestion || '');
    self.LostPasswordAnswer = ko.o(Login && Login.LostPasswordAnswer || '');
    self.IsAdmin = ko.o(Login && Login.IsAdmin || false);
    self.ReceiveNewsletter = ko.o(Login && Login.ReceiveNewsletter || false);

    self._ValidatePassword = ko.c(function () {
        if (self.Password == '')
            return false;
        return self.Password == self.ReTypePassword;
    })

    self._InsertEditModal = null;

    self._ShowInsertEdit = function (Id, mode, parentModal) {
        var title = mode == 1 ? 'Edit' : 'Insert';
        title = mode == 2 ? 'View' : title;
        if (_InsertEditModal) {
            _InsertEditModal.hide();
        }
        InsertEditLoginModal = $.modal({
            url: "/ShareholderLogin/EditCreate",
            type: 'POST',
            title: title,
            resizable: true,
            border: true,
            draggable: true,
            width: 900,
            complete: function () {
                shareholderLoginPage = new PageModel();
                if (Id) {
                    $.ajax({
                        url: "/ShareholderLogin/GetData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            //self._InsertEditModal.setModalContent(data);
                            //self._InsertEditModal.unblock();
                            InsertEditLoginModal.setModalContent(data);
                            InsertEditLoginModal.unblock();
                        }
                    }).success(function (data) {
                        if (data && typeof (data.success) != 'undefined' && data.success && data.data) {
                            var item = new ShareholderLogin(data.data);
                            //item._InsertEditModal = self._InsertEditModal;
                            item._Mode(mode);
                            shareholderLoginPage.CurrentItem(item);
                            //ko.applyBindings(shareholderLoginPage, self._InsertEditModal[0]);
                            //self._InsertEditModal.unblock();
                            ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                            InsertEditLoginModal.unblock();
                            self._LoadShareholderGrid();
                        } else {
                            var item = new ShareholderLogin(null);
                            //item._InsertEditModal = self._InsertEditModal;
                            shareholderLoginPage.CurrentItem(item);
                            //ko.applyBindings(shareholderLoginPage, self._InsertEditModal[0]);
                            //self._InsertEditModal.unblock();
                            ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                            InsertEditLoginModal.unblock();
                        }
                    });
                } else {
                    var item = new ShareholderLogin(null);
                    //item._InsertEditModal = self._InsertEditModal;
                    shareholderLoginPage.CurrentItem(item);
                    //ko.applyBindings(shareholderLoginPage, self._InsertEditModal[0]);
                    //self._InsertEditModal.unblock();
                    ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                    InsertEditLoginModal.unblock();
                }
            },
            buttons: {
                "Save": function () {
                    self._SaveData(function (data) {
                        //This will be executed when save is successfull
                        if (_InsertEditModal) {
                            _InsertEditModal.show();
                        }
                        if (data && parentModal) {
                            data.ShareholderLogin = data;
                            parentModal.ShareholderLogin(new ShareholderLogin(data));
                        }


                    });
                },
                "Close": function () {
                    //self._InsertEditModal.closeModal();
                    InsertEditLoginModal.closeModal();
                    if (_InsertEditModal) {
                        _InsertEditModal.show();
                    }
                }
            }
        });
        InsertEditLoginModal.block({ message: 'loading...' });
        //$(InsertEditLoginModal).center();
    }

    self._AddExistingUser = function (parent) {
        var shareholderSearch = new ShareholderSearch(null, parent);
        shareholderSearch._Search();
    }

    self._ScrollToTop = function () {
        $(".modal-content").animate({ scrollTop: 0 }, 'fast');
    }

    self._BindValidationError = function (serverData, clientData, modalWindow) {
        $(modalWindow).find(".input-validation-error").removeClass("input-validation-error");
        $(modalWindow).find(".tab-validation-error").removeClass("tab-validation-error");
        if (serverData) {
            clientData.ValidationErrors.removeAll();
            if (typeof (serverData.ValidationErrors) == "undefined") {
                //If not a model with ValidationErrors, then put the actual content as the validation message.
                clientData.ValidationErrors.push(new ValidationModel({ ErrorMessage: serverData }));
            } else {
                for (var error in serverData.ValidationErrors) {
                    clientData.ValidationErrors.push(new ValidationModel(serverData.ValidationErrors[error]));
                    var selector = "[id='" + serverData.ValidationErrors[error].Key + "']";
                    $(selector).addClass("input-validation-error");
                }
            }
            //This gets the grandparent of any controls with input-validation-error, and then looks for a tab with the same id, and adds the tab-validation-error into it.
            $(modalWindow).find(".input-validation-error").parents("[id]").each(function () {
                $("a[href='#" + $(this).attr("id") + "']").addClass("tab-validation-error");
            });
        }
    }

    self._SaveData = function (callback) {
        $(".modal-window").select(); //Force blur of any selected items so model updates.
        //self._InsertEditModal.block({ message: 'Saving....' });
        InsertEditLoginModal.block({ message: 'Saving....' });
        var item = shareholderLoginPage.CurrentItem();

        var d = ko.toJSON(item, function (key, value) {
            if (isNaN(key)) {
                if (key.substring(0, 1) == "_") {
                    return;
                } else {
                    return value;
                }
            }
            else {
                return value;
            }
        });
        $.ajax({
            url: "/ShareholderLogin/SaveData"
            , type: "POST"
            , datatype: "json"
            , contentType: "application/json charset=utf-8"
            , data: d
        }).success(function (result) {
            if (typeof (result.success) != "undefined" && result.success) {
                if (typeof (mygrid) != "undefined") {
                    mygrid.RefreshGrid(function () {
                        //self._InsertEditModal.closeModal();
                        InsertEditLoginModal.closeModal();
                        $("#GridMessage").text("Data Saved");
                        $("#GridMessage").show("fast");
                        var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                    });
                } else {
                    //self._InsertEditModal.closeModal();
                    InsertEditLoginModal.closeModal();
                }
                if (callback) {
                    callback(result.Data);
                }
            } else {
                //self._BindValidationError(result.data, shareholderLoginPage.CurrentItem(), self._InsertEditModal);
                self._BindValidationError(result.data, shareholderLoginPage.CurrentItem(), InsertEditLoginModal);
                self._ScrollToTop();
                //self._InsertEditModal.unblock();
                InsertEditLoginModal.unblock();
            }


        }).error(function (data) {
            InsertEditLoginModal.setModalContent(data);
            InsertEditLoginModal.unblock();
            //if (callback) {
            //    callback();
            //}
        });
    };
}

function ShareholderSearch(data, parent) {
    var self = this;
    self.ValidationErrors = ko.oA([]);
    self.ID = data && data.Id || emptyGUID;
    self.UserName = ko.o(data && data.UserName || '');
    self.EmailAddress = ko.o(data && data.EmailAddress || '');
    self.IsAdmin = ko.o(data && data.IsAdmin || false);
    self.ReceiveNewsletter = ko.o(data && data.ReceiveNewsletter || false);
    self._IsSelected = ko.o(false);
    self._SearchResult = ko.oA([])
    self._Search = function () {
        if (_InsertEditModal) {
            _InsertEditModal.hide();
        }

        SearchUserModal = $.modal({
            url: "/ShareholderLogin/Search",
            type: 'POST',
            title: 'Search',
            resizable: true,
            border: true,
            draggable: true,
            width: 600,
            complete: function () {
                //$(SearchUserModal).center();
                $('#btnAdd').parent().attr('data-bind', 'attr:{disabled:searchPage.CurrentItem()._DisableButton()}');
                var shareholderSearch = new ShareholderSearch(null, parent);
                searchPage = new PageModel();
                searchPage.CurrentItem(shareholderSearch)
                ko.applyBindings(searchPage, SearchUserModal[0]);
                SearchUserModal.unblock();
            },
            buttons: {
                "<span id='btnAdd'>Add": function () {
                    if (parent && searchPage && searchPage.CurrentItem()) {
                        var selectedItem = searchPage.CurrentItem()._SelectedLogin();
                        if (selectedItem) {
                            var d = ko.toJS(selectedItem, function (key, value) {
                                if (isNaN(key)) {
                                    if (key.substring(0, 1) == "_") {
                                        return;
                                    } else {
                                        return value;
                                    }
                                }
                                else {
                                    return value;
                                }
                            });
                            var data = {};
                            data.ShareholderLogin = d;
                            parent.ShareholderLogin(new ShareholderLogin(data));
                        }
                    }
                    SearchUserModal.closeModal();
                    if (_InsertEditModal) {
                        _InsertEditModal.show();
                    }

                },

                "Close": function () {
                    SearchUserModal.closeModal();
                    if (_InsertEditModal) {
                        _InsertEditModal.show();
                    }
                }
            }
        });
        SearchUserModal.block({ message: 'loading...' });


    }
    self._SearchUser = function () {
        self._SearchResult.removeAll();
        var searchType = $('#searchtype').val();
        var searchKwd = $('#keyword').val();
        var data = { "searchType": searchType, "kwd": searchKwd };
        var d = JSON.stringify(data)
        $.ajax({
            url: "/ShareholderLogin/SearchData"
            , type: "POST"
            , datatype: "json"
            , contentType: "application/json charset=utf-8"
            , data: d
        }).success(function (result) {
            self._DisplayMessage(false);
            if (result && result.success) {
                for (var i in result.data) {
                    var searchres = new ShareholderSearch(result.data[i]);
                    self._SearchResult.push(searchres);
                }
                if (result.data.length == 0) {
                    self._DisplayMessage(true);
                }

            } else {
                self._DisplayMessage(false);
                $('#error').text(result.Message);
            }


        }).error(function (data) {
            self._DisplayMessage(false);
            self._SearchResult.removeAll();
            SearchUserModal.setModalContent(data);
            SearchUserModal.unblock();

        });
    }
    self._DisableButton = ko.c(function () {
        var disabled = true;
        ko.utils.arrayForEach(self._SearchResult(), function (model) {
            if (model && model._IsSelected()) {
                disabled = null;
            }
        })
        return disabled;
    });
    self._SelectedLogin = function () {
        var obj = null;
        for (var i in self._SearchResult()) {
            var item = self._SearchResult()[i];
            if (item._IsSelected()) {
                obj = item;
                break;
            }
        }
        return obj;
    }
    self._DisplayMessage = ko.o(false);
}