﻿var mygrid;
var emptyGUID = '00000000-0000-0000-0000-000000000000';
var ko = null;
var shareholderPage;
var shareholderLoginPage;
var searchPage;
var InsertEditLoginModal;
var _InsertEditModal;
var SearchUserModal;
var deleteModal;
$(document).ready(function () {
    mygrid = new UgliiGrid();
    mygrid.SortBy = "";
    mygrid.IndexURL = "/ShareholderLogin/Index";
    mygrid.InitGrid();

    ko.o = ko.observable;
    ko.oA = ko.observableArray;
    ko.c = ko.computed;

});

var PageModel = function () {
    var self = this;
    self.CurrentItem = ko.o();
}

function ShareholderLogin(data) {
    var self = this;
    var Login = data && data.ShareholderLogin || null;
    self.ValidationErrors = ko.oA([]);
    self._CurrentTab = ko.o("LoginDetails");
    self._SetTab = function (val) {
        self._CurrentTab(val);
    }
    self.ID = Login && Login.ID || emptyGUID;
    self._Mode = ko.o(0);
    self.UserName = ko.o(Login && Login.UserName || '');
    self.Password = ko.o(Login && Login.Password || '');
    self.ReTypePassword = ko.o(Login && Login.Password || '');
    self.EmailAddress = ko.o(Login && Login.EmailAddress || '');
    self.LostPasswordQuestion = ko.o(Login && Login.LostPasswordQuestion || '');
    self.LostPasswordAnswer = ko.o(Login && Login.LostPasswordAnswer || '');
    self.IsAdmin = ko.o(Login && Login.IsAdmin || false);
    self.ReceiveNewsletter = ko.o(Login && Login.ReceiveNewsletter || false);

    self._ValidatePassword = ko.c(function () {
        if (self.Password == '')
            return false;
        return self.Password == self.ReTypePassword;
    })

    self._InsertEditModal = null;

    self._ShowInsertEdit = function (Id, mode, parentModal) {
        var title = mode == 1 ? 'Edit' : 'Insert';
        title = mode == 2 ? 'View' : title;
        if (_InsertEditModal) {
            _InsertEditModal.hide();
        }
        InsertEditLoginModal = $.modal({
            url: "/ShareholderLogin/EditCreate",
            type: 'POST',
            title: title,
            resizable: true,
            border: true,
            draggable: true,
            width: 900,
            complete: function () {
                $('#btnSave').parent().attr('data-bind', 'attr:{style:shareholderLoginPage.CurrentItem()._Mode()==2?\'display:none;\': null}');
                shareholderLoginPage = new PageModel();
                if (Id) {
                    $.ajax({
                        url: "/ShareholderLogin/GetData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            //self._InsertEditModal.setModalContent(data);
                            //self._InsertEditModal.unblock();
                            InsertEditLoginModal.setModalContent(data);
                            InsertEditLoginModal.unblock();
                        }
                    }).success(function (data) {
                        if (data && typeof (data.success) != 'undefined' && data.success && data.data) {
                            data.ShareholderLogin = data.data
                            var item = new ShareholderLogin(data);
                            item._Mode(mode);
                            shareholderLoginPage.CurrentItem(item);
                            ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                            InsertEditLoginModal.unblock();
                        } else {
                            var item = new ShareholderLogin(null);
                            shareholderLoginPage.CurrentItem(item);
                            ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                            InsertEditLoginModal.unblock();
                        }
                    });
                } else {
                    var item = new ShareholderLogin(null);
                    shareholderLoginPage.CurrentItem(item);
                    ko.applyBindings(shareholderLoginPage, InsertEditLoginModal[0]);
                    InsertEditLoginModal.unblock();
                }
            },
            buttons: {
                "<span id='btnSave'/>Save": function () {
                    self._SaveData(function (data) {
                        //This will be executed when save is successfull
                        if (_InsertEditModal) {
                            _InsertEditModal.show();
                        }
                        if (data && parentModal) {
                            data.ShareholderLogin = data;
                            parentModal.ShareholderLogin(new ShareholderLogin(data));
                        }


                    });
                },
                "Close": function () {
                    //self._InsertEditModal.closeModal();
                    InsertEditLoginModal.closeModal();
                    if (_InsertEditModal) {
                        _InsertEditModal.show();
                    }
                }
            }
        });
        InsertEditLoginModal.block({ message: 'loading...' });
    }

    self._AddExistingUser = function (parent) {
        var shareholderSearch = new ShareholderSearch(null, parent);
        shareholderSearch._Search();
    }

    self._ScrollToTop = function () {
        $(".modal-content").animate({ scrollTop: 0 }, 'fast');
    }

    self._BindValidationError = function (serverData, clientData, modalWindow) {
        $(modalWindow).find(".input-validation-error").removeClass("input-validation-error");
        $(modalWindow).find(".tab-validation-error").removeClass("tab-validation-error");
        if (serverData) {
            clientData.ValidationErrors.removeAll();
            if (typeof (serverData.ValidationErrors) == "undefined") {
                //If not a model with ValidationErrors, then put the actual content as the validation message.
                clientData.ValidationErrors.push(new ValidationModel({ ErrorMessage: serverData }));
            } else {
                for (var error in serverData.ValidationErrors) {
                    clientData.ValidationErrors.push(new ValidationModel(serverData.ValidationErrors[error]));
                    var selector = "[id='" + serverData.ValidationErrors[error].Key + "']";
                    $(selector).addClass("input-validation-error");
                }
            }
            //This gets the grandparent of any controls with input-validation-error, and then looks for a tab with the same id, and adds the tab-validation-error into it.
            $(modalWindow).find(".input-validation-error").parents("[id]").each(function () {
                $("a[href='#" + $(this).attr("id") + "']").addClass("tab-validation-error");
            });
        }
    }

    self._SaveData = function (callback) {
        $(".modal-window").select(); //Force blur of any selected items so model updates.
        //self._InsertEditModal.block({ message: 'Saving....' });
        InsertEditLoginModal.block({ message: 'Saving....' });
        var item = shareholderLoginPage.CurrentItem();

        var d = ko.toJSON(item, function (key, value) {
            if (isNaN(key)) {
                if (key.substring(0, 1) == "_") {
                    return;
                } else {
                    return value;
                }
            }
            else {
                return value;
            }
        });
        $.ajax({
            url: "/ShareholderLogin/SaveData"
            , type: "POST"
            , datatype: "json"
            , contentType: "application/json charset=utf-8"
            , data: d
        }).success(function (result) {
            if (typeof (result.success) != "undefined" && result.success) {
                if (typeof (mygrid) != "undefined") {
                    mygrid.RefreshGrid(function () {
                        //self._InsertEditModal.closeModal();
                        InsertEditLoginModal.closeModal();
                        $("#GridMessage").text("Data Saved");
                        $("#GridMessage").show("fast");
                        var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                    });
                } else {
                    //self._InsertEditModal.closeModal();
                    InsertEditLoginModal.closeModal();
                }
                if (callback) {
                    callback(result.Data);
                }
            } else {
                //self._BindValidationError(result.data, shareholderLoginPage.CurrentItem(), self._InsertEditModal);
                self._BindValidationError(result.data, shareholderLoginPage.CurrentItem(), InsertEditLoginModal);
                self._ScrollToTop();
                //self._InsertEditModal.unblock();
                InsertEditLoginModal.unblock();
            }


        }).error(function (data) {
            InsertEditLoginModal.setModalContent(data);
            InsertEditLoginModal.unblock();
            //if (callback) {
            //    callback();
            //}
        });
    };

    self._DeleteData = function (Id, name) {
        deleteModal = $.modal({
            //content: "<div id=\"warning\"><div class=\"warning-icon\"><div class=\"warning-heading\"><p>Delete Shareholder Login </p><div class=\"warning-content\">" + name + "</div></div></div></div>"
            url: '/ShareholderLogin/Delete/?name=' + name
            , title: 'Delete'
            , width: 800
		    , draggable: true
            , resizable: true
            , complete: function () {
                deleteModal.block();
                $.ajax({
                    url: "/ShareholderLogin/Shareholders/" + Id
                    , type: "POST"
                    , datatype: "json"
                    , contentType: "application/json charset=utf-8"
                    , data: d
                }).success(function (result) {
                    if (result && result.success) {
                        var search = new ShareholderSearch(null);
                        ko.applyBindings(search, deleteModal[0]);
                        for (var i in result.data) {
                            var searchres = new ShareholderSearch(result.data[i]);
                            search._SearchResult.push(searchres);
                        }
                    } else {
                        $('#error').text(result.Message);
                    }
                    deleteModal.unblock();
                }).error(function (data) {
                    deleteModal.setModalContent(data);
                    deleteModal.unblock();
                });
            }
            , buttons: {
                'Delete': function (win) {
                    win.setModalContent('Deleting...', true);
                    deleteModal.block();
                    $.ajax({
                        url: "/ShareholderLogin/DeleteData/" + Id,
                        type: 'POST',
                        error: function (data) {
                            win.setModalContent(data);
                            win.unblock();
                        }
                    }).success(function (data) {
                        if (data.success) {
                            if (typeof (mygrid) != "undefined") {
                                mygrid.RefreshGrid(function () {
                                    _InsertEditModal.closeModal();
                                    $("#GridMessage").text("Shareholder Deleted");
                                    $("#GridMessage").show("fast");
                                    var x = setInterval(function () { clearTimeout(x); $("#GridMessage").hide("slow"); }, 20000);
                                });
                            }
                            win.closeModal();
                        } else {
                            win.setModalContent(data.message);
                        }
                        deleteModal.closeModal();
                        win.unblock();

                    });
                }
              , 'Cancel': function (win) {
                  win.unblock();
                  deleteModal.closeModal();
              }
            }
        });
    }
}


function ShareholderSearch(data) {
    var self = this;
    self.ValidationErrors = ko.oA([]);
    self.Id = data.Id || emptyGUID;
    self.ShareholderName = ko.o(data && data.ShareholderName || '');
    self.MobilePhone = ko.o(data && data.MobilePhone || '');
    self.Town = ko.o(data && data.Town || '');
    self.Country = ko.o(data && data.Country || '');
    self._SearchResult = ko.oA([]);
    self._DisplayEmptyMessage = ko.c(function () {
        var display = true;
        if (self._SearchResult && self._SearchResult.length > 0) {
            display = false;
        }
        return display;
    });
}

