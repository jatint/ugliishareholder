/*  Version 1.16 - Test JT

Modified 2011-02-25 V1.00 SAL
Implemented Column Filtering
Fixed Column Sorting by moving the event binding of the sort boxes after the grid has been displayed.
Modified 2011-03-05 V1.01 SAL
Fixed filtering for IE. Ripped out jQuery option as it didnt' work in IE. Implemented raw string processing of XML.
Modifield 2011-03-14 V1.02 SAL
Added variable for the name of the grid, so alternate name can be used, especially if multiple grids used on pages.
Converted to an object so can be used for multiple grids.
Modified 2011-03-26 V1.03 SAL
Fixed paging. changed this. to theobj. because "this" context is lost.
Modified 2011-03-28 V1.04 SAL
Reset the page number if the filter is changed.
Modified 2011-03-31 V1.05 SAL
Fixed page jumping to the top when the user clicks on the filter image.
Modified 2011-04-08 V1.05 SAL
Fixed page number selection from dropdown - was using "this" whos context is lost, changed to "theobj"
Modified 2011-04-11 V1.06 SAL
Added "ufield" parameter, to allow for the inclusion of html-unencoded data where the JSON includes html to export.
usage:  var mygrid = new UgliiGrid();
mygrid.GridDivID = "#alternategridname";
mygrid.InitGrid();
Modified 2012-03-14 V1.07 SAL
Updated so it works with JS Compression.
Modified 2012-03-27 V1.08 SAL
Updated so that the alternating background colors are applied using a class, rather than as inline style, as this 
causes problems when another css tries to overwrite it in IE.
Classes are: oddrow and evenrow.
Modified 2012-03-28 V1.09 CW
Add callback data for RefreshGrid function. Therefore any other javascript call 
RefreshGrid(function(data) {}); can access the data values.
Modified 2012-04-05 V1.10 CW
Add customrows for customized table contents.  
Add NoRowsMessage for customized no result message.
Modified 2012-05-25 V1.11 SL
Fixed issue where data is null, and shows error that "length" is invalid
Added test that callback is not undefined
Added callback to the InitGrid so the caller can define code to run after initialization.
Added new fiel feature, where the format of the field can include a length parameter, by using the format {field:ColumnName[20]}  - in which case only 20 characters of the data will be rendered.
Modified 2012-06-18 V1.12 SL
-  Added OnRefresh function, which can be set so that whenever the grid is reloaded, it will be called after the RefreshGrid has been called. It will be called before the callback routine parameter
on the RefreshGrid function.
Modifiedl 2012-07-30 V1.12a SL 
- Moved the FillSelect and Clear Select into this JS so its together.
Modified 2012-08-03 V1.12b SL
- Updated Firefox hack so that all %7D and %7B are replaced, not just the first one.
Modified 2012-08-14 V1.12C JT
- Changed the Div class to which the grid write the message to. Chnaged from "message" to "gridMessage"
the reason for the change is that it interferes with validation summary error div class. specially in locator edit create screen 
- the WindowType can now be set to anything but main and popup to bypass the loading screen.
if this value is set to anything but popup then it will skip the loading screen
Modified 2012-8-28 V1.12D SL
--Added code so that if JSON data is /Date(xxxx)/ then it formats as a proper date.
Modified 2012-09-27 V1.12E SL
-- Added code for filter box, so it autoselects the field when loaded, and also applies when user hits the Enter button.
-- Requires new version of jquery.modal.js (which has a new event handler called onRender
Modified 2012-10-08 V1.12F SL
Updated so that the filter / sorting still works after the grid has been used in a popup page using the same JS. This attaches the grid name to the Jquery selector for finding the filter/sorting objects.
Modified 2012-11-13 V1.13G SL
  Added code to show the total records in the paging control (class=spTotalRecords) rather than the bottom of the page where its not seen.

Modified 2014-06-27 v1.14 Prabu
Removed automatic tip creation on 'a' elements.

Modified 2014-07-17 Prabu v1.15
Added functionality for dependent filter dropdowns.
Added functionality for controlling filter modal options.

Modified 2014-11-26 Prabu v1.16
Option to control the visibility of loading overlay when the RefreshGrid func is called. [UGLIIFIND-652]

=== TEST ===

*/
function UgliiGrid() {
    this.gridhtml;
    this.gridheader = "";
    this.gridrow = "";
    this.gridfooter = "";
    this.filterXML = '<f></f>';  //EG'<f><c n="colname"><v v="val1"/><v v="val1"/></c><c n="val3"><v v="val4"/></c></f>';
    this.IndexURL;
    this.GridDivID = "#grid"; //Set this variable in calling page if another id is used.
    this.PageNumberDropdownID = "#ddPage";
    this.PageNumberDropdownIDAlt = "";
    this.PageCount = "#displayPages";
    // These two variables are defined so that if the grid is in a popup, it will show a different loading layer
    this.WindowType = "main";
    this.LoadingDivID;
    this.customrows = "";
    this.NoRowsMessage = "{SID:GRID_NoRecordsFound}";
    this.f;
    this.filterType;
    this.Page = 0;
    this.Pages = 1;
    this.RowsPerPage = 20;
    this.SortBy = "";
    this.SortOrder_Asc = "0";
    this.SortOrder_Desc = "1";
    this.SortOrder = this.SortOrder_Asc; /*0=asc, 1=desc*/
    this.Rows = 0;
    this.filterModalOptions = {};
    //Overload this to enable things to happen after the grid refreshes.
    this.OnRefresh = function () { };
    this.InitGrid = function (callback) {
        var theobj = this; //Track this, because the jquery changes "this" context.
        if ((typeof (this.filterType) != "undefined") && (this.filterType == "5")) {
            this.f = this.LoadFilterArrayFromXMLForMultiSelect(this.filterXML);
        } else {
            this.f = this.LoadFilterArrayFromXML(this.filterXML);
        }
        $(this.PageNumberDropdownID).change(function () {
            theobj.Page = parseInt($(theobj.PageNumberDropdownID).val()); //SL20110408
            theobj.RefreshGrid(function () { });
        });
        $("a.pLL").click(function () {
            if (theobj.Page > 0) { theobj.Page = 0; theobj.RefreshGrid(function () { }); return false; }
        });
        $("a.pL").click(function () {
            if (theobj.Page > 0) { theobj.Page--; theobj.RefreshGrid(function () { }); return false; }
        });
        $("a.pR").click(function () {
            if (theobj.Page < theobj.Pages - 1) { theobj.Page++; theobj.RefreshGrid(function () { }); return false; }
        });
        $("a.pRR").click(function () {
            if (theobj.Page < theobj.Pages - 1) { theobj.Page = theobj.Pages - 1; theobj.RefreshGrid(function () { }); return false; }
        });
        this.gridhtml = $(this.GridDivID).html();
        this.gridheader = $(this.GridDivID).find("thead").html();
        this.gridrow = $(this.GridDivID).find("tbody").html();
        this.gridfooter = $(this.GridDivID).find("tfoot").html();
        this.RefreshGrid(callback);
    };

    this.LoadFilterArrayFromXML = function (fx) {
        var c = fx.split("<c ");
        var f = new Array();
        for (var i = 1; i < c.length; i++) {
            var d = c[i].split("<v ");
            col = d[0].replace("n=\"", "").replace("\"/>", "").replace("\">", "");
            fields = new Array();
            for (var j = 1; j < d.length; j++) {
                d[j] = d[j].replace("v=\"", "").replace("\"/>", "").replace("</c>", "").replace("</f>", "");
                fields[j - 1] = d[j];
            }
            f[col] = { fields: fields };
        }
        return f;
    };

    this.LoadFilterArrayFromXMLForMultiSelect = function (fx) {
        var c = fx.split("<c ");
        var f = new Array();
        var result = new Array();
        for (var i = 1; i < c.length; i++) {
            var d = c[i].split("<v ");
            col = d[0].replace("n=\"", "").replace("\"/>", "").replace("\">", "");
            fields = new Array();
            for (var j = 1; j < d.length; j++) {
                d[j] = d[j].replace("v=\"", "").replace("\"/>", "").replace("</c>", "").replace("</f>", "");
                result[j - 1] = d[j];
            }
            fields[0] = result;
            f[col] = { fields: fields };
        }
        return f;
    }

    this.LoadFilterXMLFromArray = function (fa) {
        var s = "<f>";
        for (var a in fa) {
            s += "<c n=\"" + a + "\">";
            if (fa[a] != null && fa[a].fields != null) {
                for (var i = 0; i < fa[a].fields.length; i++) {
                    s += "<v v=\"" + fa[a].fields[i] + "\"/>";
                }
            }
            s += "</c>";
        }
        s += "</f>";
        return s;
    };

 this.LoadFilterXMLFromArrayForMultiSelect = function (fa) {
        var s = "<f>";
        for (var a in fa) {
            s += "<c n=\"" + a + "\">";
            if (fa[a] != null && fa[a].fields != null) {
                for (var i = 0; i < fa[a].fields[0].length; i++) {
                    s += "<v v=\"" + fa[a].fields[0][i] + "\"/>";
                }
            }
            s += "</c>";
        }
        s += "</f>";
        return s;
    };

    this.RefreshGrid = function(callback, noeffects) {
        var theobj = this;
        if (!noeffects) {
            if (theobj.WindowType == "main")
                $("body").addEffectLayer({ message: "{SID:GLOBAL_Loading}" });
            else if (theobj.WindowType == "popup")
                $(theobj.LoadingDivID).addEffectLayer({ message: "{SID:GLOBAL_Loading}" });
        }

        if (theobj.filterType != "5")
            $.ajax({
                url: this.IndexURL,
                type: "POST",
                data: { p: this.Page, rp: this.RowsPerPage, f: this.LoadFilterXMLFromArray(this.f), s: this.SortBy, so: this.SortOrder },
                error: function(data) { document.write(data); }, //REPLACE WITH COMMON ERROR HANLDER.
                context: this,
                success: function(data) {
                    theobj.RenderGrid(data);
                    /* Note CSS thinks 1st row is even, jQuery thinks it is odd !! go figure!!! */
                    $(".table tbody tr:odd").addClass("evenrow");
                    $(".table tbody tr:even").addClass("oddrow");
                    this.OnRefresh();
                    if (!(callback === undefined)) callback(data);
                }
            });
        else
            $.ajax({
                url: this.IndexURL,
                type: "POST",
                data: { p: this.Page, rp: this.RowsPerPage, f: this.LoadFilterXMLFromArrayForMultiSelect(this.f), s: this.SortBy, so: this.SortOrder },
                error: function(data) { document.write(data); }, //REPLACE WITH COMMON ERROR HANLDER.
                context: this,
                success: function(data) {
                    theobj.RenderGrid(data);
                    /* Note CSS thinks 1st row is even, jQuery thinks it is odd !! go figure!!! */
                    $(".table tbody tr:odd").addClass("evenrow");
                    $(".table tbody tr:even").addClass("oddrow");
                    this.OnRefresh();
                    if (!(callback === undefined)) callback(data);
                }
            });
    };

    this.RenderGrid = function (data) {
        var gridrows = "";
        if (typeof (data) != "undefined" && typeof (data["rows"]) != "undefined" && data["rows"] != null) {
            this.Pages = parseInt(data["pages"]);
            this.Rows = parseInt(data["totalRows"]);
            this.RowsPerPage = parseInt(data["rp"]);
            this.filterXML = data["filter"];
            this.SortBy = data["sortby"];
            this.SortOrder = data["sortorder"];
			
			//This will fix the issue on the paging when a record is deleted.
            if ((this.Rows / this.RowsPerPage) <= this.Page && this.Page != 0)
                this.Page = this.Page - 1;
			
            for (var row = 0; row < data["rows"].length; row++) {
                var pos = 0;
                //Attempt to load again incase it wasn't ready before.
                if (!this.gridrow) {
                    this.gridrow = $(this.GridDivID).find("tbody").html();
                }
                var currentrow = this.gridrow;
                if (currentrow) {
                    currentrow = currentrow.replace(/%7B/gi, "{").replace(/%7D/gi, "}"); //Firefox hack !!
                    while (pos < currentrow.length && pos > -1) {
                        pos = currentrow.indexOf("{field:", pos + 1);
                        if (pos > -1) {
                            var pose = currentrow.indexOf("}", pos);
                            var bfield = currentrow.substr(pos + 7, pose - pos - 7);
                            var afield = bfield.split("[");
                            var field = afield[0];
                            var textlength = null;
                            var fielddata = String(this.HtmlEncode(data["rows"][row][field]));
                            if (fielddata.indexOf("/Date(") > -1) {
                                fielddata = (new Date(parseInt(fielddata.substr(6)))).toISOString();
                            }
                            if (afield.length > 1 && afield[1]) {
                                textlength = afield[1].split(']')[0];
                            } else {
                                textlength = fielddata.length;
                            }
                            var dispdata = typeof (fielddata) == "undefined" ? "" : fielddata.substr(0, textlength) + (textlength < fielddata.length ? "..." : "");
                            currentrow = currentrow.replace("{field:" + bfield + "}", dispdata);
                        }
                    }
                    var pos = 0;
                    while (pos < currentrow.length && pos > -1) {
                        pos = currentrow.indexOf("{ufield:", pos + 1);
                        var pose = currentrow.indexOf("}", pos);
                        var bfield = currentrow.substr(pos + 8, pose - pos - 8);
                        var afield = bfield.split("[");
                        var field = afield[0];
                        var textlength = null;
                        var fielddata = String(data["rows"][row][field]);
                        if (fielddata.indexOf("/Date(") > -1) {
                            fielddata = (new Date(parseInt(fielddata.substr(6)))).toISOString();
                        }
                        if (afield.length > 1 && afield[1]) {
                            textlength = afield[1].split(']')[0];
                        } else {
                            textlength = fielddata.length;
                        }
                        var dispdata = typeof (fielddata) == "undefined" ? "" : fielddata.substr(0, textlength) + (textlength < fielddata.length ? "..." : "");
                        currentrow = currentrow.replace("{ufield:" + bfield + "}", dispdata);
                    }
                    gridrows += currentrow;
                }
            }
            var select = new Array();
            var i = 0;
            if (this.Pages > 20) {
                startpage = this.Page > 10 ? (this.Page > this.Pages - 10 ? this.Pages - 20 : this.Page - 10) : 1;
                endpage = this.Page > 10 ? (this.Page > this.Pages - 10 ? this.Pages - 1 : this.Page + 10) : 20;
                select[i++] = { Selected: this.Page == 0, Value: 0, Text: 1 };
                select[i++] = { Selected: false, Value: -1, Text: "-----" };
                for (var p = startpage; p < endpage; p++) {
                    select[i++] = { Selected: this.Page == p, Value: p, Text: p + 1 };
                }
                select[i++] = { Selected: false, Value: -1, Text: "-----" };
                select[i++] = { Selected: this.Page == this.Pages - 1, Value: this.Pages - 1, Text: this.Pages };
            } else {
                for (var p = 0; p < this.Pages; p++) {
                    select[i++] = { Selected: this.Page == p, Value: p, Text: p + 1 };
                }
            }
            $(this.PageNumberDropdownID).fillPageSelect(select);

            if ($(this.PageNumberDropdownIDAlt).val() != "") {
                $(this.PageNumberDropdownIDAlt).fillPageSelect(select);
            }

        }
        var html = "<table class=\"table\" width=\"100%\"><thead>" + this.gridheader + "</thead><tbody>" + this.customrows + gridrows + "</tbody><tfoot>" + this.gridfooter + "</tfoot></table>";
        $(this.GridDivID).html(html);
        BindActionButtons(); //Defined in standard.js to make action buttons clickable

        /* if class is sorting, then look for the placeholder {column:xxx} and replace with the sort block.*/
        var theobj = this; //Track this, because the jquery changes "this" context.
        $(theobj.GridDivID + " .sorting").each(function () {
            var headtext = $(this).html();
            var pos2 = headtext.indexOf("{column:", 0);
            var pos2e = headtext.indexOf("}", pos2);
            var field = headtext.substr(pos2 + 8, pos2e - pos2 - 8);
            headtext = headtext.replace("{column:" + field + "}", "<span class=\"column-sort\"> <a href=\"#\" title=\"{SID:Grid_SortUp}\" data=\"" + field + "\" class=\"sort-up\"></a><a href=\"#\" data=\"" + field + "\" title=\"{SID:Grid_SortDown}\" class=\"sort-down\"></a></span>");
            $(this).html(headtext);
            if (theobj.SortBy == field) {
                if (theobj.SortOrder == theobj.SortOrder_Asc) {
                    $(this).removeClass("sorting").addClass("sorting_asc");
                } else {
                    $(this).removeClass("sorting").addClass("sorting_desc");
                }
            }

        });
        /* If class is filtering, look for filter placeholder and determine the filter control to display in the table */
        $(theobj.GridDivID + " .filtering").each(function () {
            var headtext = $(this).html();
            var filterbypos = headtext.indexOf("{filter:", 0);
            var filterbypose = headtext.indexOf("}", filterbypos);
            var filterstring = headtext.substr(filterbypos + 8, filterbypose - filterbypos - 8);
            // Note, filter string consists of format "0,FieldToFilter,ReferenceList"
            // Note that Field to Filter may be different to the displayed field
            // Reference list will return a Json list of items, with KeyValue and DisplayValue
            var filtercmd = filterstring.split(",");
            isFiltered = false;
            isFiltered = typeof (theobj.f[filtercmd[1]]) != "undefined" && theobj.f[filtercmd[1]] != null && theobj.f[filtercmd[1]].fields.length > 0;

            headtext = headtext.replace("{filter:" + filterstring + "}", "<div class=\"filter-arrow\"><a href=\"#\" class=\"filter\" data=\"" + filterstring + "\" ><img src=\"/Content/images/filter_" + (isFiltered ? "on" : "off") + ".png\" ></a></div>");
            $(this).html(headtext);
        });

        $(theobj.GridDivID).show();
        var message;
        var numPages;
        if (theobj.Rows == 0) {
            message = theobj.NoRowsMessage;
            numPages = "";
        } else {
            message = "{SID:GRID_ShowingRowXofY|StartRow=<start>|EndRow=<end>|TotalRows=<total>}".replace("<start>", (((theobj.Page) * (theobj.RowsPerPage)) + 1)).replace("<end>", ((((theobj.Page + 1) * theobj.RowsPerPage)) > theobj.Rows ? theobj.Rows : (((theobj.Page + 1) * theobj.RowsPerPage)))).replace("<total>", theobj.Rows);
            numPages = "of <pages>".replace("<pages>", theobj.Pages);
        }
        $(theobj.PageNumberDropdownID).parent().parent().find(".spTotalRecords").html("Total Records:" + theobj.Rows);
        $(".gridMessage").html(message);
        $(".displayPages").html(numPages);
        $(".loading-mask").fadeAndRemove();
        $(theobj.GridDivID + " .filter").click(function (e) {
            theobj.ShowFilter(e.currentTarget.getAttribute("data"), e.pageX, e.pageY);
            return false;
        });
        $(theobj.GridDivID + " .sort-up").click(function () {
            theobj.SortBy = $(this).attr("data");
            theobj.SortOrder = theobj.SortOrder_Asc;
            theobj.RefreshGrid(function () { });
        });
        $(theobj.GridDivID + " .sort-down").click(function () {
            theobj.SortBy = $(this).attr("data");
            theobj.SortOrder = theobj.SortOrder_Desc;
            theobj.RefreshGrid(function () { });
        });
    };
    
    /* When user changes the search parameters, then the page is reset to 1, this is to avoid
    the issue of the user going to a page number greater than the number of pages resulting from 
    the new search */
    this.resetPage = function () { this.Page = 0; };

    this.ShowFilter = function (filterstring, x, y) {
        var filtercmd = filterstring.split(",");

        /* This gets the type of filter that is rendered to the client */
        var filterType = filtercmd[0];
        /* Types are..
        0 = text box
        1 = dropdown list, single select
        2 = dropdown list, multiple select
        3 = Value range, eg 2 text boxes
        5 = dropdown list, multi-select  

        This is the field that will be filtered in the DB */
        var filterField = filtercmd[1];

        /* This determines if there is a reference set, applies to type=1 and type=2 */
        var filterRef = "";
        if (filtercmd.length > 2) { filterRef = filtercmd[2]; }

        var filterParams = [];
        if (filtercmd.length > 3) filterParams = filtercmd[3].split("|");

        var contentHTML = "";
        switch (filterType) {
            case "0":
                var filtervalue = typeof (this.f[filterField]) == "undefined" || this.f[filterField] == null ? "" : this.f[filterField].fields[0];
                filtervalue = typeof (filtervalue) == "undefined" ? "" : filtervalue;
                contentHTML += "<input type=\"text\" id=\"filtervalue\" value=\"" + filtervalue + "\">";
                this.ShowModalForFilter(contentHTML, filterType, filterField, null, null);
                break;
            case "1":
                var filtervalue1 = typeof (this.f[filterField]) == "undefined" || this.f[filterField] == null ? "" : this.f[filterField].fields[0];
                filtervalue1 = typeof (filtervalue1) == "undefined" ? "" : filtervalue1;
                contentHTML += "<select id=\"filtervalue\" ><option value=\"\">Loading..</option></select>";
                $.ajax({
                    url: "/ReferenceData/List/",
                    type: "POST",
                    data: {
                        field: filterRef,
                        args: (function (f) { return $.map(filterParams, function (v) { return f[v] && f[v].fields[0]; }); })(this.f)
                    },
                    //REPLACE WITH COMMON ERROR HANLDER.
                    error: function (data) { document.write(data); },
                    success: function (data) {
                        $("#filtervalue").fillPageSelect(data);
                        $("#filtervalue").val(filtervalue1);
                    }
                });
                this.ShowModalForFilter(contentHTML, filterType, filterField, null, null);
                break;
            case "4":
                if (typeof ($().BrandPicker) == "undefined") {
                    alert("Brand Picker not defined");
                } else {
                    this.ShowModalForFilter("<div class=\"brandpicker\" style=\"width:800px; height: 400px;\"/><script>OpenBrandPicker()</script>", filterType, filterField, null
                        , function () { //Pre Apply.
                            var r = BrandPicker.Complete();
                            return r[0].Id;
                        });
                    this.FilterModal.setModalContentSize(800, 400);
                }
            case "5":
                var filtervalue1 = typeof  (this.f[filterField]) == "undefined" || this.f[filterField] == null ? "" : this.f[filterField].fields[0];
                filtervalue1 = typeof (filtervalue1) == "undefined" ? "" :filtervalue1;
                contentHTML += "<div>{SID:Hold_CTRL}</div>";
                contentHTML += "<select id =\"filtervalue\" multiple=\"multiple\" style=\"width: 300px; height:90px;\" ><option value=\"\">Loading..</option></select>";

                $.ajax({
                    url: "/ReferenceData/List/",
                    type: "POST",
                    data: "field=" + filterRef,
                    error: function (data) { document.write(data); },
                    success: function(data) {
                        $("#filtervalue").fillPageSelect(data);
                        $("#filtervalue").val(filtervalue1);
                    }
                });
                this.ShowModalForFilter(contentHTML, filterType, filterField, null, null);
                break;
        };
    };

    this.FilterModal = null;
    this.ShowModalForFilter = function (contentHTML, filterType, filterField, completeCallBack, applyCallBack) {
        /* This determines the name of the reference data to load for the column */
        var theobj = this; //Track this as jquery replaces its context.
        this.FilterModal = $.modal($.extend({
            title: "{SID:Global_GridFilter_FilterColumnBy}"
              , content: contentHTML
              , onRender: function (win) {
                  $("#filtervalue").select();
                  $("#filtervalue").keypress(function (e) {
                      if (e.which == 13) {
                          theobj.ApplyFilter(theobj, filterType, win, filterField, applyCallBack);
                          return false;
                      }
                  });
              }
		      , complete: function () {
		          if (completeCallBack != null) {
		              completeCallBack();
		          }
		      }
            , buttons: {
                '{SID:Global_GridFilterApply}': function (win) { theobj.ApplyFilter(theobj, filterType, win, filterField, applyCallBack); },
                '{SID:Global_GridFilterClear}': function (win) {
                    /* Clear all the filter options for this column */
                    theobj.f[filterField] = null;
                    win.closeModal();
                    theobj.RefreshGrid(function () {
                    });
                },
                '{SID:Global_GridFilterClose}': function (win) { win.closeModal(); }
            }
        }, this.filterModalOptions));
    };

    this.ApplyFilter = function (theobj, filterType, win, filterField, applyCallBack) {
        /* Save the filter option to the filterJSON */
        var result = null;
        if (applyCallBack != null) {
            result = applyCallBack();
        }
        var fb;

        if(filterType == "5")
        {
            fb = theobj.LoadFilterXMLFromArrayForMultiSelect(theobj.f);   
        }
        else
        {
             fb = theobj.LoadFilterXMLFromArray(theobj.f);   
        }

        if (theobj.f[filterField] == null) { theobj.f[filterField] = { fields: new Array() } }
        switch (filterType) {
            case "0":
                result = theobj.HtmlEncode($("#filtervalue").val());
            case "1":
                result = theobj.HtmlEncode($("#filtervalue").val());
            case "5":
                result = theobj.HtmlEncode($("#filtervalue").val());
        }
        theobj.f[filterField].fields[0] = result;
        /* If filter has changed then reset the page number */
        var fa; 
        if (filterType == "5") {
            fa = theobj.LoadFilterXMLFromArrayForMultiSelect(theobj.f);
        }
        else {
            fa = theobj.LoadFilterXMLFromArray(theobj.f);
        }
        if (fa != fb) {
            theobj.Page = 0;
        }
        theobj.filterType = filterType;
        theobj.FilterModal.closeModal();
        theobj.RefreshGrid(function () {
        });
    };

    this.HtmlEncode = function (htmlstring) {
        if (typeof (htmlstring) != "undefined") {
            if (typeof (htmlstring) == "string") {
                htmlstring = htmlstring.replace(/&/gi, "&amp;").replace(/</gi, "&lt;").replace(/>/gi, "&gt;").replace(/\"/gi, "&quot;");
            }
        }
        return htmlstring;
    };
};

/* ADDED BY Cw 2012-06-08 
Moved into UgliiGrid by SL 2012-07-30
*/
$.fn.fillPageSelect = function (data) {
    return this.clearPageSelect().each(function () {
        if (this.tagName == 'SELECT') {
            var dropdownList = this;
            var selectedValue;
            $.each(data, function (index, optionData) {
                var option = new Option(optionData.Text, optionData.Value, optionData.Selected);
                if (optionData.Selected) {
                    selectedValue = optionData.Value;
                }
                if ($.browser.msie) {
                    dropdownList.add(option);
                }
                else {
                    dropdownList.add(option, null);
                }
            });
            $(this).val(selectedValue);
        }
    });
};

$.fn.clearPageSelect = function () {
    return this.each(function () {
        if (this.tagName == 'SELECT') {
            this.options.length = 0;
        }
    });
};

var BrandPicker;
function OpenBrandPicker() {

    BrandPicker = $(".brandpicker").BrandPicker({
        onComplete: function (result) {
            return result;
        }
    });
};