/* Note, this works in conjunction with the action menu html defined here..
    http://confluence.uglii.com:8090/display/SISSDOCO/3.16+-+Uglii+Grid+Technical+Information
*/
$(document).ready(function(){
    BindActionButtons();
});

var IsStillHover = false;
function BindActionButtons() {
    //Make sure we don't bind multiple clicks to the same button, so remove existing clicks first.
    $(".menu-opener.menu, .with-menu .childbreadcrumb").hide();
    $('.menu-opener').unbind("click").click(function () {
        //Edit button in Locator doesn't work with the following lines. See SL if you find a situation where these are required.
        $(this).find(".menu").show();
    });
    $('.breadcrumb .with-menu .menu').unbind("click").click(function () {
        //Edit button in Locator doesn't work with the following lines. See SL if you find a situation where these are required.
        $(this).find(".childbreadcrumb").show();
    });
    $(document).mouseup(function () {
        $(".menu-opener .menu").hide();
        $(".with-menu .childbreadcrumb").hide();

    });
}


