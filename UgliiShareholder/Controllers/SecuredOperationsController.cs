﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using UgliiShareholder.DataModel;
using UgliiShareholder.Security;
using UgliiShareholder.Utilities;
using UgliiShareholder.ViewModel;

namespace UgliiShareholder.Controllers
{
    public class SecuredOperationsController : BaseController
    {
        public SecuredOperationsController() { }


        #region "Controller Actions"


        [AcceptVerbs(HttpVerbs.Get)]
        [SecuredAction]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [SecuredAction]
        public ActionResult Index(int p, string f, string s, int so)
        {
            Expression<Func<SecuredOperation, bool>> filter = ExtensionMethods.LoadExpressionFromFilterXML<SecuredOperation>(f);
            IQueryable<SecuredOperation> shareholders = DB.SecuredOperations.Where(filter);

            int totalRows = shareholders.Count();
            var l2 = shareholders.OrderBy(s, so == 0).Skip(p * RESULTROWSPERPAGE).Take(RESULTROWSPERPAGE).ToList();

            var jResult = Json(new
            {
                sortby = s,
                sortorder = so,
                filter = f,
                rows = l2,
                pages = Math.Ceiling(totalRows / (double)RESULTROWSPERPAGE),
                page = p,
                totalRows = totalRows,
                rp = RESULTROWSPERPAGE
            }, JsonRequestBehavior.AllowGet);
            return jResult;
        }

        [HttpPost]
        [SecuredAction]
        public ActionResult EditCreate()
        {
            return View();
        }

        [HttpPost]
        [SecuredAction]
        public JsonResult GetData(Guid Id)
        {
            try
            {
                SecuredOperationsModel objSecuredOp = new SecuredOperationsModel(DB, Id);
                return Json(new { success = true, data = objSecuredOp });

            }
            catch (Exception ex)
            {
                LogManager.Debug("SecuredOperationsController/GetData :" + ex.Message);
                return Json(new { success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        [SecuredAction]
        public JsonResult SaveData(SecuredOperationsModel Data)
        {

            if (!ModelState.IsValid)
            {
                Data.GetValidationErrorsFromViewModel(ModelState);
                return Json(new { success = false, data = Data }, JsonRequestBehavior.AllowGet);
            }


            try
            {
                if (Data.Id == Guid.Empty)
                {   //Insert Mode
                    SecuredOperation objSecOp = Data.GetDatabaseModel();
                    objSecOp.ID = Guid.NewGuid();
                    DB.SecuredOperations.InsertOnSubmit(objSecOp);
                }
                else
                {   //Update Mode

                    SecuredOperation objSecOp = DB.SecuredOperations.SingleOrDefault(r => r.ID == Data.Id);
                    Data.GetDatabaseModel(objSecOp);
                }

                DB.SubmitChanges();

                return Json(new { success = true, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Error("SecuredOperationsController/SaveData: " + ex.Message);
                return Json(new { success = false, Message = "Database Could not be updated" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion


    }
}