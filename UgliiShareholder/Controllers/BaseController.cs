﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using UgliiShareholder.DataModel;
using UgliiShareholder.Utilities;

namespace UgliiShareholder.Controllers
{
    public abstract class BaseController : Controller
    {
        #region "Static Fields"

        public static readonly ILog LogManager = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static int RESULTROWSPERPAGE = 20;

        public const int MIN_PASSWORD_LENGTH = 8;

        public const int MIN_USERCODE_LENGTH = 4;

        #endregion

        public ShareHolderDataContext DB;
        private UserSession _UserSession;

        /// <summary>
        /// Overloaded constructor with dependant repositories. Repositories are injected via the IoC container.
        /// </summary>
        public BaseController()
        {
            //authService = ServiceLocator.Current.GetInstance<IAuthorizationService>();
            DB = new ShareHolderDataContext();
            //Get the current user for setting CreatedBy Id's in any objects that are created.
            //UserSession will be null if the request is loading an file such as an image. Refer. BaseController.GetUserSession(IDataContext DB)
            if (UserSession != null)
            {
                DB.CurrentUserId = UserSession.RegisteredUserID ?? Guid.Empty;
            }
        }

        private UserSession GetUserSession()
        {
            System.Web.HttpContext CurrentContext = System.Web.HttpContext.Current;
            bool UserSessionHasChanged = false;

            UserSession us = null;
            Guid UserSessionId = Guid.Empty;

            var Session = CurrentContext.Session;
            if (CurrentContext != null 
                && !CurrentContext.Request.Url.ToString().Contains(".htm")
                && !CurrentContext.Request.Url.ToString().Contains(".html")
                && !CurrentContext.Request.Url.ToString().Contains(".css")
                && !CurrentContext.Request.Url.ToString().Contains(".js")
                && !CurrentContext.Request.Url.ToString().Contains(".png")
                && !CurrentContext.Request.Url.ToString().Contains(".jpg")
                && !CurrentContext.Request.Url.ToString().Contains(".gif")
                && !CurrentContext.Request.Url.ToString().Contains(".ico")
                )
            {
                Guid.TryParse(CurrentContext.User.Identity.Name, out UserSessionId);
                if (UserSessionId == Guid.Empty)
                {
                    return new UserSession(DB); //Return a new usersession for current user.
                }
                else
                {
                    if (us == null) //Only load if UserSession is not an object.
                    {
                        us = DB.UserSessions.FirstOrDefault(u => u.ID == UserSessionId && (u.Deleted ?? false) != true);
                    }
                    if (us == null) //If User session is still not an object, then create a new one.
                    {
                        return null;
                    }
                    //If the language is not set, then well set it based on the language in the browser.
                    //UserSessionHasChanged = UpdateUserSessionWithLanguage(us) || UserSessionHasChanged;
                    //Update the session info, but only do it once per request, as this method is called multiple times
                    //per request, updating the DB each time will become inefficient.
                    if (!CurrentContext.Items.Contains("APPSET") || UserSessionHasChanged)
                    {
                        SetUserSessionApplication(us);
                        if (!System.Web.HttpContext.Current.Items.Contains("APPSET"))
                        {
                            System.Web.HttpContext.Current.Items.Add("APPSET", true);
                        }
                    }
                    if (Session != null)
                    {
                        Session["USERSESSIONID"] = us.ID;
                    }
                }
            }

            return us;
        }

        /// <summary>
        /// Object to maintain the securedoperation for a users session.
        /// </summary>
        public class SecurityToken
        {
            public Guid RegisteredUserID;
            public IList<string> SecuredOperations;
            public Guid UserSessionId;
        }

        public bool HasAccessToOperation(string operationKey)
        {
            UserSession AuthSession = UserSession;
            SecurityToken st = null;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && AuthSession != null)
            {
                System.Web.SessionState.HttpSessionState Session = System.Web.HttpContext.Current.Session;
                if (Session["SECURITY"] == null
                    || (Session["SECURITY"] as SecurityToken) == null)
                {
                    Guid RegisteredUserID = AuthSession.RegisteredUserID ?? Guid.Empty;

                    st = new SecurityToken();
                    st.UserSessionId = AuthSession.ID;
                    st.RegisteredUserID = RegisteredUserID;
                    if (AuthSession.Shareholder_Login.IsAdmin)
                    {
                        st.SecuredOperations = DB.SecuredOperations.Where(s => s.IsAdminFunction == true && s.Deleted == false).Select(s => s.operationkey.ToLower()).ToList<string>();
                    }
                    else
                    {
                        st.SecuredOperations = DB.SecuredOperations.Where(s => s.IsAdminFunction == false && s.Deleted == false).Select(s => s.operationkey.ToLower()).ToList<string>();
                    }

                    Session["SECURITY"] = st;
                }
                st = Session["SECURITY"] as SecurityToken;
                bool result = false;
                if (st != null && st.SecuredOperations != null)
                {
                    result = st.SecuredOperations.Contains<string>(operationKey.ToLower());
                }
                return result;
            }
            else
            {
                return false;
            }
        }

        void SetUserSessionApplication(UserSession us)
        {
            System.Web.HttpContext CurrentContext = System.Web.HttpContext.Current;
            /* Created 2011-08-22 SAL 
                Created as a new function to tidy up the GetUserSession method.
             */
            us.IsWebApplication = true;
            if (CurrentContext != null && CurrentContext.Request != null && CurrentContext.Request.Browser != null && CurrentContext.Request.Browser[string.Empty] != null)
            {
                us.ServerIPAddress = CurrentContext.Request.ServerVariables["LOCAL_ADDR"];
            }
        }

        public UserSession UserSession
        {
            get
            {
                bool saveSession = false;
                if (_UserSession == null || _UserSession.Shareholder_Login == null)
                {
                    if (System.Web.HttpContext.Current == null)
                    {
                        _UserSession = new UserSession(DB) { LastLoginTimeStamp = DateTime.UtcNow };
                        saveSession = true;
                    }
                    //SL20121102 - Keep the session in the HttpContext, so it carries through all the controllers avoiding multiple calls to
                    if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Items["RequestSession"] != null)
                    {
                        _UserSession = (UserSession)System.Web.HttpContext.Current.Items["RequestSession"];
                        saveSession = false; //Session is already in this request, so don't save.
                    }
                    else
                    {
                        _UserSession = (UserSession)GetUserSession();
                        if (System.Web.HttpContext.Current != null)
                        {
                            if (System.Web.HttpContext.Current.Items.Contains("RequestSession"))
                            {
                                System.Web.HttpContext.Current.Items["RequestSession"] = _UserSession;
                            }
                            else
                            {
                                System.Web.HttpContext.Current.Items.Add("RequestSession", _UserSession);
                            }
                        }
                        saveSession = true;
                    }
                    if (_UserSession != null)
                    {
                        //Only update the time if it isn't the heartbeat has it will interfere with the heartbeat time check.
                        string u = System.Web.HttpContext.Current.Request.Url.ToString();
                        if (!u.Contains("/Account/Heartbeat") && !u.Contains("/Account/UpdateLastAccessedTime"))
                        {
                            _UserSession.LastLoginTimeStamp = DateTime.UtcNow;
                        }
                    }
                    else
                    {
                        _UserSession = new UserSession(DB) { LastLoginTimeStamp = DateTime.UtcNow };
                        saveSession = true;
                    }
                    if (saveSession)
                    {
                        DB.SubmitChanges();
                    }

                }
                return _UserSession;
            }
            set
            {
                Session["USERSESSIONID"] = value.ID;
                _UserSession = value;
            }
        }

        public Shareholder_Login CurrentContextUser
        {
            get
            {
                if (UserSession == null)
                {
                    return null;
                }
                else
                {
                    return UserSession.Shareholder_Login;
                }
            }
            set
            {
                UserSession.Shareholder_Login = value;
            }
        }

        #region "Password Handling"

        [ValidateInput(false)]
        public virtual bool SetPassword(string password, out string pwHash, out string pwSalt)
        {
            if (!GeneratePasswordHashAndSalt(password, out pwHash, out pwSalt))
            {
                return false;
            }

            return true;
        }


        [ValidateInput(false)]
        public static bool GeneratePasswordHashAndSalt(string password, out string hash, out string salt)
        {
            if (!ValidatePasswordStrength(password))
            {
                hash = string.Empty;
                salt = string.Empty;
                return false;
            }

            SecurityUtils.ComputeHash(password, out hash, out salt);

            return true;
        }

        [ValidateInput(false)]
        public static bool ValidatePasswordStrength(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return false;
            }

            return (password.Length >= MIN_PASSWORD_LENGTH);
        }

        #endregion

        /// <summary>
        /// Returns True, if user name is not taken
        /// </summary>
        /// <param name="userName">username to check</param>
        /// <param name="id">user id (check whether the same username is used by other users)</param>
        /// <returns>True or False</returns>
        public bool IsUserNameAvailable(string userName, Guid id)
        {
            return DB.Shareholder_Logins.Where(l => l.UserName.ToLower() == userName.Trim().ToLower() && l.ID != id).Count() == 0;
        }


    }
}
