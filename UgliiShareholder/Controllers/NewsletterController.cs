﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using UgliiShareholder.DataModel;
using UgliiShareholder.Security;
using UgliiShareholder.Utilities;
using UgliiShareholder.ViewModel;

namespace UgliiShareholder.Controllers
{
    public class NewsletterController: BaseController
    {

        #region "Controller Actions"

        [AcceptVerbs(HttpVerbs.Get)]
        [SecuredAction]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [SecuredAction]
        public ActionResult Index(int p, string f, string s, int so)
        {
            Expression<Func<Shareholder_Detail, bool>> filter = ExtensionMethods.LoadExpressionFromFilterXML<Shareholder_Detail>(f);
            IQueryable<Shareholder_Detail> shareholders = DB.Shareholder_Details.Where(sh => sh.Deleted == false).Where(filter);

            int totalRows = shareholders.Count();
            var l2 = shareholders.OrderBy(s, so == 0).Skip(p * RESULTROWSPERPAGE).Take(RESULTROWSPERPAGE)
                 .Select(sh => new
                 {
                     Id = sh.ID,
                     ShareholderName = sh.ShareholderName,
                     MobilePhone = sh.MobilePhone ?? "",
                     EmailAddress = sh.Shareholder_Login.EmailAddress ?? "",
                     State = sh.State ?? "",
                     Country = sh.Country ?? "",
                     PostalAddress = sh.PostalAddress ?? ""
                 }
                 );

            var jResult = Json(new
            {
                sortby = s,
                sortorder = so,
                filter = f,
                rows = l2,
                pages = Math.Ceiling(totalRows / (double)RESULTROWSPERPAGE),
                page = p,
                totalRows = totalRows,
                rp = RESULTROWSPERPAGE
            }, JsonRequestBehavior.AllowGet);
            return jResult;
        }


        [HttpPost]
        [SecuredAction]
        public JsonResult GetData(Guid Id)
        {
            try
            {
                ShareHolderDetailModel objShareHolderDetail = new ShareHolderDetailModel(DB, Id);
                return Json(new { success = true, data = objShareHolderDetail });

            }
            catch (Exception ex)
            {
                LogManager.Debug("ShareHolderController/GetData :" + ex.Message);
                return Json(new { success = false, Message = ex.Message });
            }
        }


        [HttpPost]
        [SecuredAction]
        public JsonResult SaveData(ShareHolderDetailModel Data)
        {
            RemoveLoginValidationError();

            if (Data.ShareholderLogin == null || Data.ShareholderLogin.ID == Guid.Empty)
            {
                ModelState.AddModelError("ShareholderLogin", "Shareholder login information missing");
            }
            if (!ModelState.IsValid)
            {
                Data.GetValidationErrorsFromViewModel(ModelState);
                return Json(new { success = false, data = Data }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                if (Data.Id == Guid.Empty)
                {   //Insert Mode
                    Shareholder_Detail objShareDetails = Data.GetDatabaseModel();
                    objShareDetails.ID = Guid.NewGuid();
                    objShareDetails.Created = DateTime.UtcNow;
                    objShareDetails.CreatedByID = UserSession.Shareholder_Login.ID;
                    objShareDetails.Deleted = false;
                    objShareDetails.Updated = DateTime.UtcNow;
                    objShareDetails.UpdatedByID = UserSession.Shareholder_Login.ID;
                    objShareDetails.LoginID = Data.ShareholderLogin.ID;

                    DB.Shareholder_Details.InsertOnSubmit(objShareDetails);
                }
                else
                {   //Update Mode

                    Shareholder_Detail objShareDetails = DB.Shareholder_Details.SingleOrDefault(r => r.ID == Data.Id);
                    Data.GetDatabaseModel(objShareDetails);
                    objShareDetails.LoginID = Data.ShareholderLogin.ID;

                    objShareDetails.Updated = DateTime.UtcNow;
                    objShareDetails.UpdatedByID = UserSession.Shareholder_Login.ID;
                }

                DB.SubmitChanges();

                return Json(new { success = true, Message = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Error("ShareHolderController/SaveData: " + ex.Message);
                return Json(new { success = false, Message = "Database Could not be updated" }, JsonRequestBehavior.AllowGet);
            }
        }

        private void RemoveLoginValidationError()
        {
            foreach (var modelError in ModelState)
            {
                string propertyName = modelError.Key;

                if (propertyName.Contains("ShareholderLogin"))
                {
                    ModelState[propertyName].Errors.Clear();
                }
            }
        }


        [HttpPost]
        [SecuredAction]
        public ActionResult EditCreate()
        {
            return View();
        }

        [HttpPost]
        [SecuredAction]
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        [SecuredAction]
        public ActionResult DeleteData(Guid Id)
        {
            try
            {
                var shareholder = DB.Shareholder_Details.SingleOrDefault(d => d.ID == Id);
                if (shareholder != null)
                {
                    shareholder.Deleted = true;
                    shareholder.DeletedDate = DateTime.UtcNow;
                    DB.SubmitChanges();
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        #endregion

            

    }
}