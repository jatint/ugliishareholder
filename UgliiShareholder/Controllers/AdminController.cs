﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using UgliiShareholder.DataModel;
using UgliiShareholder.Security;
using UgliiShareholder.Utilities;
using UgliiShareholder.ViewModel;

namespace UgliiShareholder.Controllers
{

    public class AdminController : BaseController
    {
        #region "Controller Actions"

        // GET: /Admin/
        //[SecuredAction]
        [SecuredAction]
        public ActionResult ControlPanel()
        {
            return View();
        }
        
        #endregion

    }
}
