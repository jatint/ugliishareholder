﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using UgliiShareholder.DataModel;
using UgliiShareholder.Security;
using UgliiShareholder.Utilities;
using UgliiShareholder.ViewModel;

namespace UgliiShareholder.Controllers
{
    public class ShareholderLoginController : BaseController
    {
        [AcceptVerbs(HttpVerbs.Get)]
        [SecuredAction]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        [SecuredAction]
        public ActionResult Index(int p, string f, string s, int so)
        {

            Expression<Func<Shareholder_Login, bool>> filter = ExtensionMethods.LoadExpressionFromFilterXML<Shareholder_Login>(f);
            IQueryable<Shareholder_Login> shareholders = DB.Shareholder_Logins.Where(sh => sh.Deleted == false).Where(filter);
            int totalRows = shareholders.Count();

            var l2 = shareholders.OrderBy(s, so == 0).Skip(p * RESULTROWSPERPAGE).Take(RESULTROWSPERPAGE).ToList()
                 .Select(sh => new
                 {
                     ID = sh.ID,
                     UserName = sh.UserName,
                     EmailAddress = sh.EmailAddress,
                     IsAdmin = sh.IsAdmin,
                     ReceiveNewsletter = sh.ReceiveNewsletter,
                     Created = sh.Created.ToString("dd/MM/yyyy")

                 });

            var jResult = Json(new
            {
                sortby = s,
                sortorder = so,
                filter = f,
                rows = l2,
                pages = Math.Ceiling(totalRows / (double)RESULTROWSPERPAGE),
                page = p,
                totalRows = totalRows,
                rp = RESULTROWSPERPAGE
            }, JsonRequestBehavior.AllowGet);
            return jResult;
        }

        [HttpPost]
        [SecuredAction]
        public ActionResult EditCreate()
        {
            return View();
        }

        [HttpPost]
        [SecuredAction]
        public JsonResult GetData(Guid Id)
        {
            try
            {
                ShareholderLoginModel objShareHolderLogin = new ShareholderLoginModel(DB, Id);
                return Json(new { success = true, data = objShareHolderLogin });

            }
            catch (Exception ex)
            {
                LogManager.Debug("ShareHolderController/LoginGetData :" + ex.Message);
                return Json(new { success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        [SecuredAction]
        public JsonResult SaveData(ShareholderLoginModel Data)
        {
            //TryValidateModel(Data.ShareholderLogin);

            //Checking User Name and password strength
            if (Data != null)
            {
                if (!IsUserNameAvailable(Data.UserName, Data.ID))
                {
                    ModelState.AddModelError("UserName", "User Name not available!!");
                }
                if (Data.ID == Guid.Empty && !string.IsNullOrEmpty(Data.Password) && !ValidatePasswordStrength(Data.Password))
                {
                    ModelState.AddModelError("Password", string.Format("Password length must be {0} or more", MIN_PASSWORD_LENGTH));
                }
                if (Data.ID == Guid.Empty && !string.IsNullOrEmpty(Data.Password) && Data.Password != Data.ReTypePassword)
                {
                    ModelState.AddModelError("Password", "Password and Retype Password must match!");
                }
            }

            //Removing Password and Retype Password in Edit mode
            if (Data.ID != Guid.Empty)
            {
                ModelState.Remove("Password");
                ModelState.Remove("ReTypePassword");
            }


            if (!ModelState.IsValid)
            {
                Data.GetValidationErrorsFromViewModel(ModelState);
                return Json(new { success = false, data = Data }, JsonRequestBehavior.AllowGet);
            }


            try
            {
                Shareholder_Login objShareholderLogin;
                if (Data.ID == Guid.Empty)
                {   //Insert Mode
                    objShareholderLogin = Data.GetDatabaseModel();
                    Data.ID = objShareholderLogin.ID = Guid.NewGuid();
                    objShareholderLogin.Created = DateTime.UtcNow;
                    objShareholderLogin.CreatedByID = UserSession.ID;
                    string passwordHash = string.Empty;
                    string passwordSalt = string.Empty;

                    SetPassword(Data.Password, out passwordHash, out passwordSalt);
                    objShareholderLogin.PasswordHash = passwordHash;
                    objShareholderLogin.PasswordSalt = passwordSalt;
                    DB.Shareholder_Logins.InsertOnSubmit(objShareholderLogin);
                }
                else
                {   //Update Mode

                    objShareholderLogin = DB.Shareholder_Logins.SingleOrDefault(r => r.ID == Data.ID);
                    Data.GetDatabaseModel(objShareholderLogin);

                    objShareholderLogin.Updated = DateTime.UtcNow;
                    objShareholderLogin.UpdatedByID = UserSession.Shareholder_Login.ID;
                }

                DB.SubmitChanges();

                return Json(new { success = true, Data = objShareholderLogin }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogManager.Error("ShareHolderController/LoginSaveData: " + ex.Message);
                return Json(new { success = false, Message = "Database Could not be updated" }, JsonRequestBehavior.AllowGet);
            }
        }

        #region "Delete Operation"

        [HttpPost]
        [SecuredAction]
        public ActionResult Delete(string name)
        {
            ViewData["name"] = name;
            return View();
        }

        [HttpPost]
        [SecuredAction]
        public ActionResult DeleteData(Guid Id)
        {
            try
            {
                var shareholder = DB.Shareholder_Logins.SingleOrDefault(d => d.ID == Id);
                if (shareholder != null)
                {
                    shareholder.Deleted = true;
                    shareholder.DeletedDate = DateTime.UtcNow;
                    DB.SubmitChanges();
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }


        [HttpPost]
        [SecuredAction]
        public ActionResult Shareholders(Guid Id)
        {

            try
            {
                IQueryable<Shareholder_Detail> objShareholderDetails =
                    DB.Shareholder_Logins.Where(sd => sd.ID == Id).SelectMany(sd => sd.Shareholder_Details);
                
                var result = objShareholderDetails.Select(s => new
                {
                    Id = s.ID,
                    ShareholderName = s.ShareholderName,
                    MobilePhone = s.MobilePhone,
                    Town = s.Town,
                    Country = s.Country,
                });
                return Json(new { success = true, data = result });

            }
            catch (Exception ex)
            {
                LogManager.Debug("ShareHolderController/SearchData :" + ex.Message);
                return Json(new { success = false, Message = ex.Message });
            }
        }

        #endregion

        #region "For Shareholder Management"

        [HttpPost]
        [SecuredAction]
        public JsonResult SearchData(int searchType, string kwd)
        {
            try
            {
                IList<Shareholder_Login> objShareholderLogins;
                switch (searchType)
                {
                    case 1:
                        objShareholderLogins = DB.Shareholder_Logins.Where(l => l.Deleted == false && l.UserName.ToLower().Contains(kwd.ToLower()))
                            .OrderBy(p => p.UserName).Take(20).ToList();
                        break;
                    case 2:
                        objShareholderLogins = DB.Shareholder_Logins.Where(l => l.Deleted == false && l.EmailAddress.ToLower().Contains(kwd.ToLower()))
                            .OrderBy(p => p.EmailAddress).Take(20).ToList();
                        break;
                    default:
                        objShareholderLogins = DB.Shareholder_Logins.Where(l => l.Deleted == false).OrderBy(p => p.UserName).Take(20).ToList();
                        break;
                }

                var result = objShareholderLogins.Select(s => new
                {
                    Id = s.ID,
                    UserName = s.UserName,
                    EmailAddress = s.EmailAddress,
                    ReceiveNewsletter = s.ReceiveNewsletter,
                    IsAdmin = s.IsAdmin
                });
                return Json(new { success = true, data = result });

            }
            catch (Exception ex)
            {
                LogManager.Debug("ShareHolderController/SearchData :" + ex.Message);
                return Json(new { success = false, Message = ex.Message });
            }

        }

        [HttpPost]
        [SecuredAction]
        public ActionResult Search()
        {
            return View();
        }

        #endregion

    }
}