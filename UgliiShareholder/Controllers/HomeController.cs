﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UgliiShareholder.Controllers
{
    [HandleError]
    public class HomeController : BaseController
    {


        public ActionResult Index()
        {
            if(!User.Identity.IsAuthenticated || UserSession == null)
            {
                return Redirect("/Account/Index");
            }
            else
            {
                if (UserSession.Shareholder_Login.IsAdmin)
                {
                    return Redirect("/Admin/ControlPanel");
                }
                else
                {
                    return Redirect("/Home/ControlPanel");
                }
            }

            
        }

        public ActionResult ControlPanel()
        {
            return View();
        }
    }
}
