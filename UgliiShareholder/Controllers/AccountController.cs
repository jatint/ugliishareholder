﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using UgliiShareholder.DataModel;
using log4net;
using UgliiShareholder.Utilities;

namespace UgliiShareholder.Controllers
{

    [HandleError]
    public class AccountController : BaseController
    {

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string userName, string password, bool rememberMe, string returnUrl)
        {

            Session["SECURITY"] = null; //Clear so it get reloaded incase UserSession in DB data changed.

            Shareholder_Login user = null;
            if (String.IsNullOrEmpty(userName))
            {
                this.ModelState.AddModelError("username", "Username Is Required.");
            }

            if (String.IsNullOrEmpty(password))
            {
                this.ModelState.AddModelError("password", "Password Is Required.");
            }

            if (ViewData.ModelState.IsValid)
            {
                user = DB.Shareholder_Logins.FirstOrDefault(q => q.UserName == userName && !(q.Deleted));

                if (user != null)
                {

                    if (user.AccountLockedUntil > DateTime.UtcNow)
                    {
                        ModelState.AddModelError("_FORM", "You have exceeded five login attempts and have been locked-out for one hour.");
                        return View();
                    }

                    if (!SecurityUtils.VerifyHash(password, user.PasswordHash, user.PasswordSalt))
                    {
                        LogManager.WarnFormat("User: {0} login failed", user.UserName);

                        user.FailedLoginCount = (user.FailedLoginCount ?? 0) + 1;
                        if (user.FailedLoginCount > 5)
                        {
                            ModelState.AddModelError("_FORM", "You have exceeded five login attempts and have been locked-out for one hour.");
                            user.AccountLockedUntil = DateTime.UtcNow.AddHours(1);
                            user.FailedLoginCount = 0; //once the account has been locked - reset the count to 0
                        }
                        else
                        {
                            ModelState.AddModelError("_FORM", "Either the username or password is not correct");
                        }
                        DB.SubmitChanges();
                        return View();
                    }
                    else
                    {
                        LogManager.InfoFormat("User: {0} login successful", user.UserName);
                    }
                }
                else
                {
                    LogManager.WarnFormat("User: {0} not found", userName);
                    ModelState.AddModelError("_FORM", "Either the username or password is not correct");
                    return View();
                }

                if (user.AccountLockedUntil > DateTime.UtcNow)
                {
                    ModelState.AddModelError("_FORM", "You have exceeded five login attempts and have been locked-out for one hour.");
                    return View();
                }


                //Reset failed login.
                user.AccountLockedUntil = null;
                user.FailedLoginCount = 0;
                user.LastLoginDate = DateTime.UtcNow;

                UserSession us = new UserSession(DB);  //Create a new usersession for each login.
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                                                                                 us.ID.ToString(),
                                                                                 DateTime.UtcNow,
                                                                                 DateTime.UtcNow.AddHours(24),  // value of time out property
                                                                                 true,                          // Value of IsPersistent property
                                                                                 us.ID.ToString(), "/");

                string encryptedTicket = FormsAuthentication.Encrypt(ticket);
                HttpCookie authCookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    encryptedTicket);
                authCookie.Path = "/";
                authCookie.Expires = ticket.Expiration;
                Response.Cookies.Add(authCookie);

                us.RegisteredUserID = user.ID;
                us.LastLoginTimeStamp = DateTime.UtcNow;
                us.LoginDate = DateTime.UtcNow;
                us.ServerIPAddress = Request.ServerVariables["LOCAL_ADDR"];

                //Create the security token which is used to store the secured operations.
                SecurityToken sk = new SecurityToken();
                sk.UserSessionId = us.ID;
                sk.RegisteredUserID = us.RegisteredUserID ?? Guid.Empty;
                if (user.IsAdmin)
                {
                    sk.SecuredOperations = DB.SecuredOperations.Where(s => s.IsAdminFunction == true && s.Deleted == false).Select(s => s.operationkey.ToLower()).ToList<string>();
                }
                else
                {
                    sk.SecuredOperations = DB.SecuredOperations.Where(s => s.IsAdminFunction == false && s.Deleted == false).Select(s => s.operationkey.ToLower()).ToList<string>();
                }

                //JT: User is now Logged In
                DB.UserSessions.InsertOnSubmit(us);
                DB.SubmitChanges();


            }
            else
            {
                return View("Login");
            }

            ActionResult ar = null;
            ar = RedirectToAction("Index", "Home");
            return ar;
        }

        // **************************************
        // URL: /Account/LogOff
        // **************************************

        public ActionResult LogOff()
        {

            UserSession.Shareholder_Login = null;
            UserSession.Deleted = true;
            UserSession.DeleteDate = DateTime.UtcNow;

            try
            {
                //Don't care it this fails, there is no downside. Most of the time is it http headers error.
                DB.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                if (Session["SECURITY"] != null)
                {
                    Session.Remove("SECURITY");
                }
                FormsAuthentication.SignOut();
            }
            catch (Exception ex)
            {
                //Do Nothing                
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult UnauthorisedAccess()
        {
            return View();
        }

        //// **************************************
        //// URL: /Account/Register
        //// **************************************

        //public ActionResult Register()
        //{
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult Register(RegisterModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // Attempt to register the user
        //        MembershipCreateStatus createStatus = MembershipService.CreateUser(model.UserName, model.Password, model.Email);

        //        if (createStatus == MembershipCreateStatus.Success)
        //        {
        //            FormsService.SignIn(model.UserName, false /* createPersistentCookie */);
        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", AccountValidation.ErrorCodeToString(createStatus));
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View(model);
        //}

        //// **************************************
        //// URL: /Account/ChangePassword
        //// **************************************

        //[Authorize]
        //public ActionResult ChangePassword()
        //{
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View();
        //}

        //[Authorize]
        //[HttpPost]
        //public ActionResult ChangePassword(ChangePasswordModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if (MembershipService.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword))
        //        {
        //            return RedirectToAction("ChangePasswordSuccess");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    ViewData["PasswordLength"] = MembershipService.MinPasswordLength;
        //    return View(model);
        //}

        //// **************************************
        //// URL: /Account/ChangePasswordSuccess
        //// **************************************

        //public ActionResult ChangePasswordSuccess()
        //{
        //    return View();
        //}

    }
}
