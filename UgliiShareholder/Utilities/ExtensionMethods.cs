﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Xml;

namespace UgliiShareholder.Utilities
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Extension method for Orderby so that the field to order by is passed as a string.
        /// Also supports an additional parameter for the sort direction, which will dynamically choose between
        /// the OrderBy and OrderByDescending
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyName"></param>
        /// <param name="Ascending"></param>
        /// <returns></returns>
        public static IQueryable<T> OrderBy<T>(this IQueryable<T> source, string propertyName, bool Ascending)
        {
            return (IQueryable<T>)OrderBy((IQueryable)source, propertyName, Ascending);
        }

        public static IQueryable OrderBy(this IQueryable source, string propertyName, bool Ascending)
        {
            /* Modified SAL 2011-02-28 - Updated to support the passing of propertyName with child properties, eg child.field
             * but only supports 1:1 - Eg child property cannot be a collection */
            if (!String.IsNullOrEmpty(propertyName))
            {
                var x = Expression.Parameter(source.ElementType, "x");
                string[] propertytree = propertyName.Split('.');
                MemberExpression mp0 = Expression.PropertyOrField(x, propertytree[0]);
                if (propertytree.Length > 1)
                {
                    for (int i = 1; i < propertytree.Length; i++)
                        mp0 = Expression.PropertyOrField(mp0, propertytree[i]);
                }
                var selector = Expression.Lambda(mp0, x);
                return source.Provider.CreateQuery(
                  Expression.Call(typeof(Queryable), (Ascending ? "OrderBy" : "OrderByDescending"), new Type[] { source.ElementType, selector.Body.Type },
                         source.Expression, selector
                         ));
            }
            return source;
        }

        /// <summary>
        /// Takes the filterXML provided from the Uglii_Grid dynamic column filtering, and creates an Expression that can be used in the 
        /// where clause of a lambda express.
        /// Output logic is "(col1.Val1 OR col1.Val2 OR col1.Val3 OR ...) AND (col2.Val1 OR col2.Val2 OR col3.Val3))
        /// 
        /// Format of Input XML
        /// <f><c n="Col1"><v v="Val1"/><v v="Val2"/>...</c></f>
        /// </summary>
        /// <typeparam name="T">Type of the objects that Expression is to apply</typeparam>
        /// <param name="filterXML">XML containing the filter from the Client functions. Refer uglii_grid.js </param>
        /// <returns>A Lambda Expression containing the where clause logic.</returns>
        public static Expression<Func<T, bool>> LoadExpressionFromFilterXML<T>(string filterXML)
        {
            ParameterExpression p0 = Expression.Parameter(typeof(T), "c");  //Gets the c part of c=> ....
            XmlDocument xd = new XmlDocument();
            Expression andlogic = null;
            if (filterXML != String.Empty)
            {
                xd.LoadXml(filterXML.ToLower()); // S.A - ignores case sensetivity;
                XmlNodeList xnl = xd.SelectNodes("f/c[count(v) > 0]");
                foreach (XmlNode xn in xnl)
                {
                    MemberExpression mp0 = null;
                    string[] propertytree = xn.Attributes["n"].Value.ToString().Split('.');
                    mp0 = Expression.PropertyOrField(p0, propertytree[0]);
                    if (propertytree.Length > 1)
                    {
                        for (int i = 1; i < propertytree.Length; i++)
                        {
                            mp0 = Expression.PropertyOrField(mp0, propertytree[i]);
                        }
                    }
                    XmlNodeList xnlValueList = xn.SelectNodes("v");
                    Expression orlogic = null;
                    foreach (XmlNode xnValue in xnlValueList)
                    {
                        ConstantExpression f0 = null;
                        Expression cequal = null;
                        switch (mp0.Type.ToString())
                        {
                            case "System.Guid":
                                f0 = Expression.Constant(new Guid(xnValue.Attributes["v"].Value.ToString()));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Guid]":
                                f0 = Expression.Constant((Guid?)new Guid(xnValue.Attributes["v"].Value.ToString()), typeof(System.Guid?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Boolean":
                                f0 = Expression.Constant(bool.Parse(xnValue.Attributes["v"].Value.ToString()));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Boolean]":
                                f0 = Expression.Constant((Boolean?)bool.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Boolean?));
                                cequal = Expression.Equal(mp0, f0);
                                break;

                            case "System.Int32":
                                f0 = Expression.Constant(int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int32));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Int32]":
                                f0 = Expression.Constant((int?)int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int32?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Int64":
                                f0 = Expression.Constant(int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int64));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Int64]":
                                f0 = Expression.Constant((long?)long.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int64?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            //DR: Added to Support while filtering dates, work's prefect.
                            case "System.Nullable`1[System.DateTime]":
                                DateTime dateResult;
                                bool dateParsed = DateTime.TryParse(xnValue.Attributes["v"].Value.ToString(), out dateResult);

                                if (dateParsed)
                                    f0 = Expression.Constant((DateTime?)dateResult, typeof(System.DateTime?));
                                else if (String.IsNullOrEmpty(xnValue.Attributes["v"].Value.ToString().Trim()))
                                    f0 = Expression.Constant((DateTime?)null, typeof(System.DateTime?));
                                else
                                    f0 = Expression.Constant((DateTime?)DateTime.MaxValue, typeof(System.DateTime?));

                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.DateTime":
                                DateTime dateResult2;
                                bool dateParsed2 = DateTime.TryParse(xnValue.Attributes["v"].Value.ToString(), out dateResult2);

                                if (dateParsed2)
                                    f0 = Expression.Constant((DateTime)dateResult2, typeof(System.DateTime));
                                else if (String.IsNullOrEmpty(xnValue.Attributes["v"].Value.ToString().Trim()))
                                    f0 = Expression.Constant(DateTime.UtcNow, typeof(System.DateTime));
                                else
                                    f0 = Expression.Constant((DateTime)DateTime.MaxValue, typeof(System.DateTime));

                                cequal = Expression.Equal(mp0, f0);
                                break;
                            /* TODO - add other data types here */
                            default:
                                /* For a string, we use the logic of c=>c.MyProperty.Contains("the value")  as a wild card.
                                    This only applies to filterType=0 (TODO - need to implemented other filtertypes)
                                 */
                                f0 = Expression.Constant(xnValue.Attributes["v"].Value.ToString());
                                Expression cequal1 = Expression.Call(mp0, "ToLower", null, null); // S.A - ignores case sensetivity;
                                cequal = Expression.Call(cequal1, "Contains", null, f0);
                                break;
                        }

                        if (orlogic == null)
                        {
                            orlogic = cequal;
                        }
                        else
                        {
                            orlogic = Expression.OrElse(orlogic, cequal);
                        }
                    }
                    if (andlogic == null)
                    {
                        andlogic = orlogic;
                    }
                    else
                    {
                        andlogic = Expression.AndAlso(andlogic, orlogic);
                    }
                }
            }
            Expression<Func<T, bool>> lambda1 = null;
            if (andlogic == null)
            {
                /* Fake expression so that return value doesn't return a null if there are no filters, otherwise the calling lambda breaks */
                andlogic = Expression.Equal(Expression.Constant(true), Expression.Constant(true));
            }
            lambda1 = Expression.Lambda<Func<T, bool>>(andlogic, new ParameterExpression[] { p0 });
            return lambda1;
        }

        /// <summary>
        /// Takes the filterXML provided from the Uglii_Grid dynamic column filtering, and creates an Expression that can be used in the 
        /// where clause of a lambda express.
        /// Output logic is "(col1.Val1 OR col1.Val2 OR col1.Val3 OR ...) AND (col2.Val1 OR col2.Val2 OR col3.Val3))
        ///  This expression will be case insensitive for  user defined list of objects 
        /// Format of Input XML
        /// <f><c n="Col1"><v v="Val1"/><v v="Val2"/>...</c></f>
        /// </summary>
        /// <typeparam name="T">Type of the objects that Expression is to apply</typeparam>
        /// <param name="filterXML">XML containing the filter from the Client functions. Refer uglii_grid.js </param>
        /// <returns>A Lambda Expression containing the where clause logic.</returns>
        public static Expression<Func<T, bool>> LoadExpressionFromFilterXMLIgnoreCase<T>(string filterXML)
        {
            ParameterExpression p0 = Expression.Parameter(typeof(T), "c");  //Gets the c part of c=> ....
            XmlDocument xd = new XmlDocument();
            Expression andlogic = null;
            if (filterXML != String.Empty)
            {
                xd.LoadXml(filterXML);
                XmlNodeList xnl = xd.SelectNodes("f/c[count(v) > 0]");
                foreach (XmlNode xn in xnl)
                {
                    MemberExpression mp0 = null;
                    string[] propertytree = xn.Attributes["n"].Value.ToString().Split('.');
                    mp0 = Expression.PropertyOrField(p0, propertytree[0]);
                    if (propertytree.Length > 1)
                    {
                        for (int i = 1; i < propertytree.Length; i++)
                        {
                            mp0 = Expression.PropertyOrField(mp0, propertytree[i]);
                        }
                    }
                    XmlNodeList xnlValueList = xn.SelectNodes("v");
                    Expression orlogic = null;
                    foreach (XmlNode xnValue in xnlValueList)
                    {
                        ConstantExpression f0 = null;
                        Expression cequal = null;
                        switch (mp0.Type.ToString())
                        {
                            case "System.Guid":
                                f0 = Expression.Constant(new Guid(xnValue.Attributes["v"].Value.ToString()));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Guid]":
                                f0 = Expression.Constant((Guid?)new Guid(xnValue.Attributes["v"].Value.ToString()), typeof(System.Guid?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Boolean":
                                f0 = Expression.Constant(bool.Parse(xnValue.Attributes["v"].Value.ToString()));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Boolean]":
                                f0 = Expression.Constant((Boolean?)bool.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Boolean?));
                                cequal = Expression.Equal(mp0, f0);
                                break;

                            case "System.Int32":
                                f0 = Expression.Constant(int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int32));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Int32]":
                                f0 = Expression.Constant((int?)int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int32?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Int64":
                                f0 = Expression.Constant(int.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int64));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            case "System.Nullable`1[System.Int64]":
                                f0 = Expression.Constant((long?)long.Parse(xnValue.Attributes["v"].Value.ToString()), typeof(System.Int64?));
                                cequal = Expression.Equal(mp0, f0);
                                break;
                            //DR: Added to Support while filtering dates, work's prefect.
                            case "System.Nullable`1[System.DateTime]":
                                DateTime dateResult;
                                bool dateParsed = DateTime.TryParse(xnValue.Attributes["v"].Value.ToString(), out dateResult);

                                if (dateParsed)
                                {
                                    f0 = Expression.Constant((DateTime?)dateResult, typeof(System.DateTime?));
                                }
                                else if (String.IsNullOrEmpty(xnValue.Attributes["v"].Value.ToString().Trim()))
                                {
                                    f0 = Expression.Constant((DateTime?)null, typeof(System.DateTime?));
                                }
                                else
                                {
                                    f0 = Expression.Constant((DateTime?)DateTime.MaxValue, typeof(System.DateTime?));
                                }

                                cequal = Expression.Equal(mp0, f0);

                                break;
                            case "System.DateTime":
                                DateTime dateResult2;
                                bool dateParsed2 = DateTime.TryParse(xnValue.Attributes["v"].Value.ToString(), out dateResult2);

                                if (dateParsed2)
                                {
                                    f0 = Expression.Constant((DateTime)dateResult2, typeof(System.DateTime));
                                }
                                else if (String.IsNullOrEmpty(xnValue.Attributes["v"].Value.ToString().Trim()))
                                {
                                    f0 = Expression.Constant(DateTime.UtcNow, typeof(System.DateTime));
                                }
                                else
                                {
                                    f0 = Expression.Constant((DateTime)DateTime.MaxValue, typeof(System.DateTime));
                                }

                                cequal = Expression.Equal(mp0, f0);

                                break;
                            /* TODO - add other data types here */
                            default:
                                /* For a string, we use the logic of c=>c.MyProperty.ContainsExt("the value")  as a wild card.
                                    This only applies to filterType=0 (TODO - need to implemented other filtertypes)
                                 */
                                f0 = Expression.Constant(xnValue.Attributes["v"].Value.ToString());
                                cequal = Expression.Call(null, typeof(ExtensionMethods).GetMethod("ContainsExt", new Type[] { typeof(String), typeof(String) }), mp0, f0);
                                break;
                        }

                        if (orlogic == null)
                        {
                            orlogic = cequal;
                        }
                        else
                        {
                            orlogic = Expression.OrElse(orlogic, cequal);
                        }
                    }
                    if (andlogic == null)
                    {
                        andlogic = orlogic;
                    }
                    else
                    {
                        andlogic = Expression.AndAlso(andlogic, orlogic);
                    }
                }
            }
            Expression<Func<T, bool>> lambda1 = null;
            if (andlogic == null)
            {
                /* Fake expression so that return value doesn't return a null if there are no filters, otherwise the calling lambda breaks */
                andlogic = Expression.Equal(Expression.Constant(true), Expression.Constant(true));
            }
            lambda1 = Expression.Lambda<Func<T, bool>>(andlogic, new ParameterExpression[] { p0 });
            return lambda1;
        }

        public static bool ContainsExt(this String str, string val)
        {
            return str.IndexOf(val, StringComparison.InvariantCultureIgnoreCase) > -1 ? true : false;
        }
    }
}