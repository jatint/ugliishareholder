﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Security;

namespace UgliiShareholder.Utilities
{
    /// <summary>
    /// Various security related utility functions
    /// </summary>
    public class SecurityUtils
    {
        private const string HASH_ALG = "SHA1";
        private const int SALT_LENGTH = 16;
        private const string URL_IV = "Pulmonary Embolism";
        private const string URL_KEY = "Kempei Tai";
        private const string UPPER_ALPHA_SOURCE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string LOWER_ALPHA_SOURCE = "abcdefghijklmnopqrstuvwxyz";
        private const string NUMERIC_SOURCE = "1234567890";
        private const string SYMBOL_SOURCE = @" !""#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
        public const int MAX_SIGNED_INT_UPPER_BOUND = 0x7FFFFFFF; //max value for a signed int (2^31 - 1)


        private const string URLData_KEY = "=1)*&S`_";
        private const string URLData_IV = "/*-1kj#$";        


        /// <summary>
        /// Computes the hash of a supplied value
        /// </summary>
        /// <param name="plain">The plain value</param>
        /// <param name="hash">The resulting hash text</param>
        public static void ComputeHash(string plain, out string hash)
        {
            hash = FormsAuthentication.HashPasswordForStoringInConfigFile(plain, HASH_ALG);
        }

        /// <summary>
        /// Verify that a hash value is the same as a hash of a plain value
        /// </summary>
        /// <param name="plain">The plain text</param>
        /// <param name="hash">The hash text</param>
        /// <returns>Whether or not the supplied hash value matches the hash of the supplied plain value</returns>
        public static bool VerifyHash(string plain, string hash)
        {
            return hash.Equals(FormsAuthentication.HashPasswordForStoringInConfigFile(plain, HASH_ALG));
        }

        /// <summary>
        /// Computes hash and salt values for a supplied value
        /// </summary>
        /// <param name="plain">The plain text</param>
        /// <param name="hash">The hash value result</param>
        /// <param name="salt">The salt value result</param>
        public static void ComputeHash(string plain, out string hash, out string salt)
        {
            byte[] saltBytes = new byte[SALT_LENGTH];

            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            rng.GetNonZeroBytes(saltBytes);

            salt = Convert.ToBase64String(saltBytes);

            hash = FormsAuthentication.HashPasswordForStoringInConfigFile(salt + plain, HASH_ALG);
        }

        /// <summary>
        /// Verify that a hash value is the same as a hash of a plain value (+salt)
        /// </summary>
        /// <param name="plain">The plain text</param>
        /// <param name="hash">The hash text</param>
        /// <param name="salt">The salt value</param>
        /// <returns>Whether or not the supplied hash value matches the hash (+salt) of the supplied plain value</returns>
        public static bool VerifyHash(string plain, string hash, string salt)
        {
            return (hash == FormsAuthentication.HashPasswordForStoringInConfigFile(salt + plain, HASH_ALG));
        }


        /// <summary>
        /// Converts a byte array to a base64 string
        /// </summary>
        /// <param name="bytes">Byte array to convert</param>
        /// <returns>base64 string representing the supplied byte array</returns>
        public static string ToBase64String(byte[] bytes)
        {
            return Convert.ToBase64String(bytes);
        }

        /// <summary>
        /// Converts a base64 string to a byte array
        /// </summary>
        /// <param name="encoded">A base64 string</param>
        /// <returns>The byte array represented by the base64 string</returns>
        public static byte[] FromBase64String(string encoded)
        {
            return Convert.FromBase64String(encoded);
        }

        /// <summary>
        /// Converts a byte array to a base64ForUrls string
        /// </summary>
        /// <param name="bytes">Byte array to convert</param>
        /// <returns>base64ForUrls string representing the supplied byte array</returns>
        public static string ToBase64ForUrlsString(byte[] bytes)
        {
            string encoded = Convert.ToBase64String(bytes);

            encoded = encoded.Replace("/", "_").Replace("+", "-").Replace("=", "");

            return encoded;
        }

        /// <summary>
        /// Converts a base64ForUrls string to a byte array
        /// </summary>
        /// <param name="encoded">A base64ForUrls string</param>
        /// <returns>The byte array represented by the base64ForUrls string</returns>
        public static byte[] FromBase64ForUrlsString(string encoded)
        {
            string base64 = encoded.Replace("_", "/").Replace("-", "+");

            switch (base64.Length % 4)
            {
                case 1:
                    base64 += "=";
                    break;
                case 2:
                    base64 += "==";
                    break;
                case 3:
                    throw new FormatException("String is not a valid Base64ForUrls value");
            }

            return Convert.FromBase64String(base64);
        }


    }
}